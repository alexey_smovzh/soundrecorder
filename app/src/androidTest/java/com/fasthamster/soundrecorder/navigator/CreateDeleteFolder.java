package com.fasthamster.soundrecorder.navigator;


import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.fasthamster.soundrecorder.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class CreateDeleteFolder {

    private static final String TEST_FOLDER_NAME = "sample_folder";

    @Rule
    public ActivityTestRule<RecordsList> mActivityTestRule = new ActivityTestRule<>(RecordsList.class);

    @Test
    public void createFolder() {
        // Click add folder in top bar menu
        onView(withId(R.id.action_folder_add)).perform(click());
        // Input folder name in dialog and press positive button
        onView(withId(R.id.create_folder_name)).perform(typeText(TEST_FOLDER_NAME), closeSoftKeyboard());
        onView(allOf(withText(R.string.create_folder_positive_button))).perform(click());
        // Check if folder created
        onView(ViewMatchers.withId(R.id.rv_list_records)).perform(
                RecyclerViewActions.scrollToHolder(findInRecyclerViewByName(TEST_FOLDER_NAME)));
        onView(withText(TEST_FOLDER_NAME)).check(matches(isDisplayed()));
    }

    @Test
    public void deleteFolder() {

    }

    private Matcher<RecyclerView.ViewHolder> findInRecyclerViewByName(final String name) {
        return new BoundedMatcher<RecyclerView.ViewHolder, FilesCardHolder>(FilesCardHolder.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("No view found with name: " + name);
            }

            @Override
            protected boolean matchesSafely(FilesCardHolder item) {
                TextView fileName = (TextView)item.itemView.findViewById(R.id.record_title);
                if(fileName == null)
                    return false;

                return fileName.getText().toString().contains(TEST_FOLDER_NAME);
            }
        };
    }
}