package com.fasthamster.soundrecorder;

import java.util.ArrayList;

/**
 * Created by alex on 06.06.17.
 */

public class Comment {

    private long from;
    private long to;
    private String text;

    /**
     * Constructor to create subtitles from program
     * when we know all params
     * @param from start time
     * @param to end time
     * @param text message
     */
    public Comment(long from, long to, String text) {
        this.from = from;
        this.to = to;
        this.text = text;
    }

    /**
     * Constructor to create subtitle from subtitle file
     * when we need to parse file content
     * @param lines one subtitle fragment lines
     */
    public Comment(ArrayList<String> lines) {
        // first line is time mark, split it and convert to timestamps
        String[] time = lines.get(0).split(" ");
        this.from = stringToTimestamp(time[0]);
        this.to = stringToTimestamp(time[2]);

        // all next lines are subtitle text
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < lines.size(); i++)
            builder.append(lines.get(i));

        this.text = builder.toString();
    }

    // Getters
    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public String getText() {
        return text;
    }

    public String getTextFrom() {
        return timestampToString(from);
    }

    public String getTextTo() {
        return timestampToString(to);
    }

    // Setters
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Convert string to long timestamp
     * method assume that string is in "hh:mm:ss,SSS" format
     * @param string to convert
     * @return long result
     */
    private long stringToTimestamp(String string) {
        // split to hours, minutes and seconds
        String[] split = string.split(":");
        return Long.parseLong(split[0]) * 3600000 +
               Long.parseLong(split[1]) * 60000 +
               // take only two first numbers
               Long.parseLong(split[2].substring(0, 2)) * 1000;

    }

    /**
     * Convert long timestamp value to string in format "hh:mm:ss,SSS"
     * @param timestamp to convert
     * @return resulting string
     */
    private String timestampToString(long timestamp) {
        return String.format("%02d:%02d:%02d,000", timestamp/(3600*1000),
                timestamp/(60*1000) % 60,
                timestamp/1000 % 60);
    }
}
