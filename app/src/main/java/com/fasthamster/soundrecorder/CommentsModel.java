package com.fasthamster.soundrecorder;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_ADD;
import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_DELETE;
import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_EDIT;

/**
 * Created by alex on 18.04.17.
 */

public class CommentsModel extends Observable {

    private String path;
    private SoundRecorder parent;
    private ArrayList<Comment> comments = new ArrayList<Comment>();

    // Lock for save comments to file in "fairness" mode abide by the order of lock requests
    private Lock lockIO = new ReentrantLock(true);
    private final static int TOAST_WRITE_TO_FILE_ERROR = 0;


    /**
     * Handler to post message from save thread to UI
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            if(message.what == TOAST_WRITE_TO_FILE_ERROR) {
                Toast.makeText(parent, parent.getResources().getString(R.string.save_comment_io_error) + path, Toast.LENGTH_SHORT).show();
            }
        }
    };

    /**
     * Thread to save comments array to subtitles file
     * if it content not empty, overwrite it
     * @param path full path to comments file
     */
    @SuppressWarnings("JavadocReference")
    private class SaveThread extends Thread {
        private String path;
        private Comment comment;

        SaveThread(String path) {
            this.path = path;
        }

        SaveThread(String path, Comment comment) {
            this.path = path;
            this.comment = comment;
        }

        public void run() {
            lockIO.lock();
            try {
                // save only if there are some comments
                if (comments.size() != 0) {
                    boolean result;
                    StringBuilder builder = new StringBuilder();
                    // Not any comment passed so we are in full comments array save mode
                    if(comment == null) {
                        // fill builder with comments data
                        for (Comment s : comments)
                            commentBuilderAppend(builder, s);

                        // Write content to disk
                        result = parent.getStorageHelper().writeTextFile(path, builder.toString(), false);
                        // One comment passed, we are in append comments file mode
                    } else {
                        commentBuilderAppend(builder, comment);
                        result = parent.getStorageHelper().writeTextFile(path, builder.toString(), true);
                    }
                    // Saving fails show message to user
                    if (result == false)
                        handler.sendEmptyMessage(TOAST_WRITE_TO_FILE_ERROR);
                }
            } finally {
                lockIO.unlock();
            }
        }
    }

    /**
     * Format comment data to string message and append it to string builder
     * Used in save comments thread
     * @param builder string builder
     * @param comment comment
     */
    private void commentBuilderAppend(StringBuilder builder, Comment comment) {
        builder.append(comment.getTextFrom() + " --> " + comment.getTextTo() + "\n")
               .append(comment.getText() + "\n")
               .append("\n");
    }

    /**
     * Constructor
     * @param parent
     */
    public CommentsModel(SoundRecorder parent) {
        this.parent = parent;
    }

    /**
     * Load comments
     * @param path full path to comments file
     */
    public void loadComments(String path) {
        // Save path for future use
        this.path = path;
        // if comments array not empty clear it
        if(comments.size() != 0) comments.clear();
        String text = parent.getStorageHelper().readTextFile(path);
        // If comments file exists and we are readed it, parse and load subtitles
        // in other case it will created on comments save
        if(text.isEmpty() == false)
            parseSubtitles(text);

    }

    /**
     * Run thread to write comments to file
     * @param path full path to comments file
     */
    private void saveComments(String path) {
        new SaveThread(path).start();
    }

    /**
     * Run thread to append comments file
     * @param path full path to comments file
     * @param comment comment
     */
    private void saveComments(String path, Comment comment) {
        new SaveThread(path, comment).start();
    }

    /**
     * Parse comments to comments array
     * @param text comments text to parse
     */
    private void parseSubtitles(String text) {
        // Split text to lines
        String[] split = text.split("\\r?\\n");
        ArrayList<String> buf = new ArrayList<>();
        // for every line
        for (int i = 0; i < split.length; i++) {
            // if not empty add to temp buffer
            if(!split[i].isEmpty()) {
                buf.add(split[i]);
                // if empty create new Comment and clear temp buffer
            } else {
                comments.add(new Comment(buf));
                buf.clear();
            }
        }
        // if file ended not with empty line, previous cycle condition not work
        // so we check if temp buffer not empty and if it, create subtitle
        if(buf.size() != 0) {
            comments.add(new Comment(buf));
            buf.clear();
        }
    }

    /**
     * Get comments
     * @return comments
     */
    public ArrayList<Comment> getComments() {
        return comments;
    }

    /**
     * Get comment by index
     * @param index
     * @return
     */
    public Comment getComment(int index) {
        return comments.get(index);
    }

    /**
     * Add comment by the comment text
     * @param timestampTo current record time
     * @param text of comment
     */
    public void addComment(long timestampTo, String text) {
        int index = 0;
        long timestampFrom = 0L;
        boolean isAppend = false;

        if(comments.size() > 0) {
            // Comment timestampTo greater than timestampTo of last comment
            // so just append comments array list
            if(comments.get(lastIndex()).getTo() < timestampTo) {
                timestampFrom = comments.get(lastIndex()).getTo();
                index = comments.size();
                isAppend = true;
            } else {
                // We must find where insert comment in the array list
                for (int i = 0; i < comments.size(); i++) {
                    if(comments.get(i).getTo() > timestampTo) {
                        // find first value greater than timestampTo
                        // if timestampTo of this comment is the smallest, to avoid IndexOutOfBound
                        timestampFrom = comments.get(i == 0 ? 0 : i - 1).getTo();
                        index = i;
                        break;
                    }
                }
            }
        }

        Comment comment = new Comment(timestampFrom, timestampTo, text);
        comments.add(index, comment);
        setChanged();
        notifyObservers(new int[]{COMMENT_ACTION_ADD, index});

        // Save comments to disk
        // Append comments file if this is last comment
        if(isAppend == true) {
            saveComments(path, comment);
        // overwrite whole file
        } else {
            saveComments(path);
        }
    }

    /**
     * Edit comment text by index
     * @param index comment index
     * @param text new text
     */
    public void editComment(int index, String text) {
        comments.get(index).setText(text);
        setChanged();
        notifyObservers(new int[]{COMMENT_ACTION_EDIT, index});
        saveComments(path);
    }

    /**
     * Delete comment with given index
     * @param index
     */
    public void deleteComment(int index) {
        comments.remove(index);
        setChanged();
        notifyObservers(new int[]{COMMENT_ACTION_DELETE, index});
        saveComments(path);
    }

    /**
     *
     * @return index of last element in comments array
     */
    private int lastIndex() {
        return comments.size() - 1;
    }
}
