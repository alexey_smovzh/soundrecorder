package com.fasthamster.soundrecorder;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Environment;

import com.fasthamster.soundrecorder.navigator.RecordsList;

/**
 * Created by alex on 18.04.17.
 */

public class Constants {

    public static final String TAG = RecordsList.class.getName();

    // Recorder mode
    public static final byte RECORDER_ACTIVITY_PLAYING_MODE = 0;
    public static final byte RECORDER_ACTIVITY_RECORD_MODE = 1;

    // Record activity return codes
    public static final int RECORDER_ACTIVITY_RESULT = 1;
    public static final String RECORDER_ACTIVITY_EXTRA_ID = "message";
    public static final int RECORDER_ACTIVITY_CANT_RECORD = 10;
    public static final int RECORDER_ACTIVITY_WRONG_MIME_TYPE = 11;
    public static final int RECORDER_ACTIVITY_FINISH_WITH_RECORD = 12;
    public static final int RECORDER_ACTIVITY_FINISH_WITHOUT_RECORD = 13;

    public static final String DEFAULT_RECORDS_DIR = Environment.DIRECTORY_MUSIC;
    public static final String DEFAULT_RECORDS_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+DEFAULT_RECORDS_DIR;

    public static final int NO_RECORD = -1;
    public static final String RECORD_EXTRA_ID = "record_id";
    public static final String RECORD_EXTRA_DIR = "record_dir";

    public static final String RECORD_FILE_EXTENSION = ".mp4";
    public static final String COMMENT_FILE_EXTENSION = ".srt";
    public static final String TEMPORARY_FILE_NAME = "record-";
    public static final String TEMPORARY_FILE_EXTENSION = ".part";

    public static final boolean TYPE_DIRECTORY = true;
    public static final boolean TYPE_FILE = false;
// todo: change int data type to byte or short
    // count of created segment on initialization
    public static final int VISUALIZER_INITIALIZED_SEGMENTS_COUNT = 2;
    public static final int VISUALIZER_NUMBERS_TEXTURE = R.drawable.time_numbers;
    // 512 pixels texture width, 11 symbols, 64 pixels texture height
    public static final float VISUALIZER_NUMBERS_RATIO = (512f/11f/64f);
    // 100 milliseconds, i.e. 10 frames per second
    public static final int VISUALIZER_REPEAT_INTERVAL = 100;
    // max value returned by getMaxAmplitude();
    public static final short VISUALIZER_MAX_AMPLITUDE = 32767;     // e.g. Short.MAX_VALUE;
    // for quicker segments generation without freezing on slow devices
    // and for simplicity of GLTimeLabels class method addNumber
    // record split for 10 seconds intervals
    public static final int VISUALIZER_SECONDS_PER_SEGMENT = 10;
    // it equals visualizer repeat interval i.e. 10 amplitude gets per second
    public static final int VISUALIZER_AMPLITUDES_PER_SECOND = 10;
    public static final int VISUALIZER_AMPLITUDES_PER_SEGMENT = VISUALIZER_SECONDS_PER_SEGMENT * VISUALIZER_AMPLITUDES_PER_SECOND;
    // e.g. font size
    public static final float VISUALIZER_TIME_LABEL_HEIGHT = 20f;
    // begins from center of surface
    public static final float VISUALIZER_GRID_COLOR[] = { 0.6f, 0.6f, 0.6f, 1f };
    public static final float VISUALIZER_AMPLITUDE_COLOR[] = { 1f, 1f, 1f, 1f };
    public static final float VISUALIZER_PLAYED_AMPLITUDE_COLOR[] = { 1f, 0f, 0f, 1f };
    public static final float VISUALIZER_GRID_LINE_WIDTH = 3f;
    public static final float VISUALIZER_WAVE_LINE_WIDTH = 6f;
    public static final float VISUALISER_GRID_MARGIN = 0.05f;            // i.e. 5%
    public static final float VISUALISER_LABELS_MARGIN = 0.04f;          // i.e. 4%

    // visualizer buffers constants
    public static final int COORDS_PER_VERTEX = 2;  // x, y
    public static final int COLORS_PER_VERTEX = 4; // i.e. r, g, b, a
    public static final int BYTES_PER_FLOAT = 4;
    public static final int BYTES_PER_SHORT = 2;
    public static final int COORDS_PER_LINE = COORDS_PER_VERTEX * 2;
    public static final int COLORS_PER_LINE = COLORS_PER_VERTEX * 2;
    public static final int INDICES_PER_LINE = 2;
    public static final int VERTICES_STRIDE = COORDS_PER_VERTEX * BYTES_PER_FLOAT;
    public static final int COLORS_STRIDE = COLORS_PER_VERTEX * BYTES_PER_FLOAT;

    // Comments
    public static final int COMMENT_CARD_PERCENT_WIDTH = 70;
    // Action codes to pass to observer update method and let him know desired action
    public static final int COMMENT_ACTION_ADD = 0;
    public static final int COMMENT_ACTION_EDIT = 1;
    public static final int COMMENT_ACTION_DELETE = 2;
    //
    public static final int COMMENT_NO_EDITABLE_COMMENT = -1;

    // Dynamically defined constants on app start
    public static float VISUALIZER_BACKGROUND_COLOR[];


    // Attributes from current theme used dynamically in code
    public static final int[] THEME_ATTRIBUTES = { android.R.attr.windowBackground };


    public Constants(Context context) {

        TypedArray values = context.obtainStyledAttributes(R.style.AppTheme, THEME_ATTRIBUTES);
        VISUALIZER_BACKGROUND_COLOR = decodeColor(values.getColor(0, Color.BLACK));

    }

    /**
     * Converts int color value to float array
     * @param color
     * @return
     */
    private float[] decodeColor(int color) {
        return new float[] {
                (float)((color      ) & 0xff) / 255f,
                (float)((color >>  8) & 0xff) / 255f,
                (float)((color >> 16) & 0xff) / 255f,
                (float)((color >> 24) & 0xff) / 255f
        };
    }
}
