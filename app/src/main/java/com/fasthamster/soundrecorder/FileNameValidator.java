package com.fasthamster.soundrecorder;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by alex on 09.07.17.
 */

public abstract class FileNameValidator implements TextWatcher {

    private final TextView view;

    public FileNameValidator(TextView view) {
        this.view = view;
    }

    public abstract void validate(TextView view, String text);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = view.getText().toString();
        validate(view, text);
    }
}
