package com.fasthamster.soundrecorder;

import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by alex on 27.05.17.
 */


// https://stackoverflow.com/questions/13422673/looking-for-a-working-example-of-addtimedtextsource-for-adding-subtitle-to-a-vid
// https://developer.android.com/reference/android/media/MediaExtractor.html
// https://android.googlesource.com/platform/frameworks/base/+/cd92588/media/java/android/media/SubtitleController.java
// https://developer.android.com/reference/android/support/v4/media/MediaDescriptionCompat.Builder.html#setSubtitle(java.lang.CharSequence)
// https://developer.android.com/reference/android/media/MediaFormat.html
// https://developer.android.com/reference/android/media/MediaRecorder.OutputFormat.html
// https://github.com/Semantive/waveform-android/tree/master/library/src/main/java/com/semantive/waveformandroid/waveform/soundfile
// https://github.com/artclarke/humble-video

// https://stackoverflow.com/questions/2645041/ffmpeg-for-a-android-using-tutorial-ffmpeg-and-android-mk
// https://stackoverflow.com/questions/3641920/how-to-encode-a-wav-to-a-mp3-on-a-android-device/

// http://android.amberfog.com/?p=181
// https://bitbucket.org/ijabz/jaudiotagger/src/e27ae7e9430b3fa404bd54230749ed6a9a48d850/src/org/jaudiotagger/audio/?at=master
// https://stackoverflow.com/questions/17570526/extracting-amr-data-from-3gp-file
// https://github.com/sannies/mp4parser/tree/master/streaming/src/main/java/org/mp4parser/streaming/extensions

// https://en.wikipedia.org/wiki/Timed_text


// TODO: store and read comments within media file
public class MediaFilesHelper {

    private SoundRecorder parent;
    private ArrayList<Comment> comments = new ArrayList<Comment>();


    public MediaFilesHelper(SoundRecorder parent) {
        this.parent = parent;
    }

    public void help(String path) {
        // Load comments
//        String text = parent.getStorageHelper().readTextFile(path);
        String text = parent.getStorageHelper().readTextFile("/storage/sdcard/Music/test.srt");
        if(text != null)
            parseSubtitles(text);


        // Save comments
        save(path);

    }

    // todo: save comments array to subtitle file
    // if it content not empty, overwrite it
    public void save(String path) {

        StringBuilder builder = new StringBuilder();

        for(Comment s : comments) {
            builder.append(s.getTextFrom() + " --> " + s.getTextTo() + "\n");
            builder.append(s.getText() + "\n");
            builder.append("\n");
        }

        boolean result = parent.getStorageHelper().writeTextFile(path, builder.toString(), false);

        if(result == false)
            Toast.makeText(parent, "Error creating comments file: " + path, Toast.LENGTH_SHORT).show();

    }

    /**
     * Parse comments to comments array
     * @param text comments text to parse
     */
    private void parseSubtitles(String text) {
        // Split text to lines
        String[] split = text.split("\\r?\\n");
        ArrayList<String> buf = new ArrayList<>();
        // for every line
        for (int i = 0; i < split.length; i++) {
            // if not empty add to temp buffer
            if(!split[i].isEmpty()) {
                buf.add(split[i]);
            // if empty create new Comment and clear temp buffer
            } else {
                comments.add(new Comment(buf));
                buf.clear();
            }
        }
        // if file ended not with empty line, previous cycle condition not work
        // so we check if temp buffer not empty and if it, create subtitle
        if(buf.size() != 0) {
            comments.add(new Comment(buf));
            buf.clear();
        }
    }

    /**
     * Read file and get comments from it
     * @return comments
     */
    public String getComments() {
        return null;
    }

    /**
     * Writes comments to media file
     * @param comments
     */
    public void writeComments(String comments) {

    }

}
