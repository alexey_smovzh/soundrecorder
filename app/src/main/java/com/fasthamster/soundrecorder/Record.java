package com.fasthamster.soundrecorder;

import java.io.File;

import static com.fasthamster.soundrecorder.Constants.COMMENT_FILE_EXTENSION;

/**
 * Created by alex on 13.04.17.
 */
/*
 *  Record model class
 */
public class Record {

    // file name without extension
    private String name;
    private String path;
    private long size;
    private long modified;
    private boolean type;

    public Record(String name, String path, long size, long modified, boolean type) {
        this.name = name;
        this.path = path;
        this.size = size;
        this.modified = modified;
        this.type = type;
    }

    // Getters
    public String getFileName() { return name; }
    public String getFilePath() { return path; }
    public String getFullPath() {
        if(name.equals("..")) {
            return path;
        } else {
            return path + File.separator + name;
        }
    }
    // to create comment file name, in record file name replace its extension to default comment file extension
    public String getCommentName() {
        return name.replaceFirst("[.][^.]+$", "") + COMMENT_FILE_EXTENSION;
    }
    public String getFullCommentPath() {
        return path + File.separator + getCommentName();
    }
    public long getFileSize() { return size; }
    public long getModified() { return modified; }
    public boolean getType() { return type; }

}
