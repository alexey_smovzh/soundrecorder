package com.fasthamster.soundrecorder;

import java.util.List;
import java.util.Observable;

/**
 * Created by alex on 15.04.17.
 */

public class RecordsModel extends Observable {

    private SoundRecorder parent;
    private List<Record> records;

// todo: refactor like comment model, move all logic with records there
    public RecordsModel(SoundRecorder parent) {
        this.parent = parent;

//        loadRecords();
    }
/*
    public void loadRecords() {
        loadRecords(Constants.DEFAULT_RECORDS_PATH);
    }

    public void loadRecords(String path) {
        // make list of available files from shared storage folder
        // open shared storage and check if it is writable and we can save files to it
        // record files list and fill records
        if(parent.getStorageHelper().isExternalStorageWritable() == true) {
            records = parent.getStorageHelper().getStorageDir(path);
            setChanged();
            notifyObservers();
        } else {
            Toast.makeText(parent, "External storage unavailable", Toast.LENGTH_SHORT);
        }
    }
*/
    public Record getRecord(int id) {
        return records.get(id);
    }

    public List<Record> getRecords() {
        return records;
    }
    //
    public void addRecord(Record record) {
        records.add(record);
        setChanged();
        notifyObservers();
    }

    public void updateRecord(int recordId, Record record) {

    }

    public void deleteRecord(int recordId) {
        records.remove(recordId);
        setChanged();
        notifyObservers();
    }

    public void deleteComment(int recordId, int commentId) {

    }
}

