package com.fasthamster.soundrecorder;

import android.app.Application;
import android.content.Context;

/**
 * Created by alex on 18.04.17.
 */

/*
 * Class to share model class between activities to get MVC implementation
 */

// -7

public class SoundRecorder extends Application {

    private static Context context;

    RecordsModel records;
    CommentsModel comments;

    StorageHelper helper;
    Constants constants;

    @Override
    public void onCreate() {
        super.onCreate();
        // Setup context
        context = getApplicationContext();
        // Init some constants
        constants = new Constants(context);
        // Initialise storage helper
        helper = new StorageHelper(this);
        // Initialize records model
        records = new RecordsModel(this);
        // Initialize comments model
        comments = new CommentsModel(this);
    }

    public RecordsModel getRecordsModelObservable() {
        return records;
    }

    public CommentsModel getCommentsModelObservable() {
        return comments;
    }

    public StorageHelper getStorageHelper() {
        return helper;
    }

    public static Context getContext() {
        return context;
    }
}
