package com.fasthamster.soundrecorder;

import android.os.Environment;

import com.fasthamster.soundrecorder.navigator.FileHolder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 19.04.17.
 */

public class StorageHelper {

    private SoundRecorder parent;


    public StorageHelper(SoundRecorder parent) {
        this.parent = parent;
    }


    public boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);

    }
    // Create folder
    public boolean createFolder(String path) {
        File folder = new File(path);
        // if folder doesn't exists create it
        if(folder.exists() == false)
            return folder.mkdir();

        return false;
    }
    // Delete files and folders
    public boolean deleteFileFolder(String path) {
        File file = new File(path);
        if(file.exists() == true)
            return deleteRecursive(file);

        return false;
    }
    // recursive deletion
    private boolean deleteRecursive(File file) {
        if(file.isDirectory()) {
            for (File child : file.listFiles()){
                deleteRecursive(child);
            }
        }
        return file.delete();
    }

    /**
     * Rename file or folder
     * @param from current name
     * @param to new name
     * @return true if succeed false otherwise
     */
    public boolean renameFileFolder(String from, String to) {
        File f = new File(from);
        File t = new File(to);
        if(f.exists() == true)
            return f.renameTo(t);

        return false;
    }

    /**
     * Check if file exist
     * @param file full path to file
     * @return validation result
     */
    public boolean isFileExist(String file) {
       File f = new File(file);
        return f.exists() && f.isFile();
    }
// todo: move to file/folders model. to not checking always on disk, just check List of readed early files&dirs in memory
    /**
     * Check if folder exist
     * @param folder full path to folder
     * @return validation result
     */
    public boolean isFolderExist(String folder) {
        File f = new File(folder);
        return f.exists() && f.isDirectory();
    }

    /* returns list of files and folder from given path
     * to folders within sdcard directory adds two dots folder to go to upper level
     * skip hidden files and directories which name begins with dot
     */
    public List<FileHolder> getStorageDir(String path) {
        ArrayList<FileHolder> result = new ArrayList<>();

        File f = new File(path);
        File file[] = f.listFiles();

        // Directory not empty
        if(file != null) {
            for (int i = 0; i < file.length; i++) {
                // If it is a hidden file which name started with dot, skip it
                if(file[i].getName().charAt(0) == '.') continue;
                // remove extension on readed file name
                result.add(new FileHolder(file[i].getName(),
                                      path,
                                      file[i].length(),
                                      file[i].lastModified(),
                                      file[i].isDirectory()));
            }

            if(!f.getName().equalsIgnoreCase("sdcard"))
                result.add(0, new FileHolder("..", f.getParent(), 0, 0, Constants.TYPE_DIRECTORY));
        // Directory empty
        } else {
            if(!f.getName().equalsIgnoreCase("sdcard"))
                result.add(new FileHolder("..", f.getParent(), 0, 0, Constants.TYPE_DIRECTORY));
        }

        return result;
    }

    /**
     * Read text file. designed to read subtitles file
     * @param path file path
     * @return read result as String or empty string if can't read
     */
    public String readTextFile(String path) {
        String result = "";
        FileReader reader = null;
        File file = new File(path);

        if(!file.exists()) return result;
        if(!file.canRead()) return result;

        try {
            reader = new FileReader(file);
            char[] chars = new char[(int)file.length()];
            reader.read(chars);
            result = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Write subtitles to file
     * @param path subtitles file full path
     * @param text subtitles
     */
    public boolean writeTextFile(String path, String text, boolean append) {

        File file = new File(path);
        FileWriter writer = null;

        try {
            if(!file.exists()) {
                file.createNewFile();
            }
            if(!file.canWrite()) {
                return false;
            }
            // Open file in append mode end write content to end
            if(append == true) {
                writer = new FileWriter(file, true);
                writer.append(text);
            // Open file in write mode and overwrite it content if it exists
            } else {
                writer = new FileWriter(file);
                writer.write(text);
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }
}
