package com.fasthamster.soundrecorder.navigator;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.fasthamster.soundrecorder.R;

/**
 * Created by alex on 23.04.17.
 */

public class ActionBarCallback implements ActionMode.Callback {

    private RecordsList parent;


    public ActionBarCallback(RecordsList parent) {
        this.parent = parent;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.contextual_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_delete:
                parent.getFilesListPresenter().actionDeleteFile(mode);
                break;
            case R.id.item_rename:
                parent.getFilesListPresenter().actionRenameFile(mode);
                break;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mode = null;
        parent.getFilesListPresenter().clearSelected();
        parent.getFilesListPresenter().clearSelected();
        parent.getFilesListPresenter().setIsInChoiceMode(false);
    }
}
