package com.fasthamster.soundrecorder.navigator;

import java.io.File;

/**
 * Created by alex on 31.07.17.
 */

public class FileHolder {

    private String name;     // file name without extension
    private String path;
    private long size;
    private long modified;
    private boolean type;

    public FileHolder(String name, String path, long size, long modified, boolean type) {
        this.name = name;
        this.path = path;
        this.size = size;
        this.modified = modified;
        this.type = type;
    }

    // Getters
    public String getFileName() { return name; }
    public String getFilePath() { return path; }
    public String getFullPath() {
        if(name.equals("..")) {
            return path;
        } else {
            return path + File.separator + name;
        }
    }
    public long getFileSize() { return size; }
    public long getModified() { return modified; }
    public boolean getType() { return type; }

}
