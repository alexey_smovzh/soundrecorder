package com.fasthamster.soundrecorder.navigator;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasthamster.soundrecorder.R;

/**
 * Created by alex on 15.04.17.
 */

public class FilesCardHolder extends RecyclerView.ViewHolder implements FilesCardView {

    // specially for changing background color on selection
    // if set background color on card, appears ugly borders
    RelativeLayout background;
    ImageView mimeIcon;
    TextView title, size, modified;

    public FilesCardHolder(View view) {
        super(view);

        background = (RelativeLayout)view.findViewById(R.id.rl_record_card);
        mimeIcon = (ImageView)view.findViewById(R.id.iv_mime_type_icon);
        title = (TextView)view.findViewById(R.id.record_title);
        size = (TextView)view.findViewById(R.id.record_size);
        modified = (TextView)view.findViewById(R.id.record_modified);
    }

    @Override
    public void setBackgroundColor(int color) {
        this.background.setBackgroundColor(color);
    }

    @Override
    public void setMimeIcon(int icon) {
        this.mimeIcon.setImageResource(icon);
    }

    @Override
    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setSize(String size) {
        this.size.setText(size);
    }

    @Override
    public void setModified(String modified) {
        this.modified.setText(modified);
    }
}
