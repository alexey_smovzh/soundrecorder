package com.fasthamster.soundrecorder.navigator;

/**
 * Created by alex on 31.07.17.
 */

public interface FilesCardView {
    void setBackgroundColor(int color);
    void setMimeIcon(int icon);
    void setTitle(String title);
    void setSize(String size);
    void setModified(String modified);
}
