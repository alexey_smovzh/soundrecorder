package com.fasthamster.soundrecorder.navigator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasthamster.soundrecorder.Constants;
import com.fasthamster.soundrecorder.FileNameValidator;
import com.fasthamster.soundrecorder.R;
import com.fasthamster.soundrecorder.recorder.RecorderActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_RESULT;
import static com.fasthamster.soundrecorder.Constants.TYPE_DIRECTORY;

/**
 * Created by alex on 15.04.17.
 */

// todo: refactor getListeners like in CommentCardHolder

public class FilesListPresenter {

    private RecordsList parent;
    private ActionMode actionMode;
    private String defaultPath;     // Variable to save default records path

    private List<FileHolder> files;
    private List<Integer> selected = new ArrayList<>();
    private boolean isInChoiceMode;

    private FilesRecyclerListAdapter adapter;

    private TypedValue selectedBackground, normalBackground;          // store current theme highlight color


    public FilesListPresenter(RecordsList parent) {

        this.parent = parent;

        parent.getRecordsListView().registerFabRecordListener(getFabRecordListener());
        parent.getRecordsListView().registerFilesListClickListener(getRecordsListListener());
        // Set action bar title to default records storage folder
        parent.getRecordsListView().setActionBarTitle(Constants.DEFAULT_RECORDS_PATH);
        // this.view.setActionBarTitle(Constants.DEFAULT_RECORDS_DIR);
        this.defaultPath = Constants.DEFAULT_RECORDS_PATH;

        // load files list
        loadRecords();

        // register files recycler list adapter
        adapter = new FilesRecyclerListAdapter(this);
        parent.getRecordsListView().registerFilesListAdapter(adapter);

        // load background colors for normal and selected file card state from current device default theme
        selectedBackground = new TypedValue();
        normalBackground = new TypedValue();
        parent.getBaseContext().getTheme().resolveAttribute(R.attr.colorControlHighlight, selectedBackground, true);
        parent.getBaseContext().getTheme().resolveAttribute(R.attr.colorControlNormal , normalBackground, true);

    }

    private View.OnClickListener getFabRecordListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRecordActivity(Constants.NO_RECORD);
            }
        };
    }

    private RecyclerViewOnClickListener getRecordsListListener() {

        return new RecyclerViewOnClickListener(){
            @Override
            public void onClick(View v, int position) {
                // if in action mode add or remove cards from selected array
                if(getIsInChoiceMode() == true) {
                    switchSelectedState(position);
                    actionMode.setTitle(String.valueOf(getSelectedItemsCount()));
                    // if items count = 0, so all items are deselected, exit from action mode
                    if(getSelectedItemsCount() == 0)
                        actionMode.finish();
                } else {
                    FileHolder r = getFile(position);
                    if (r.getType() == TYPE_DIRECTORY) {
                        // update action bar title
                        parent.getRecordsListView().setActionBarTitle(r.getFullPath());
                        // view.setActionBarTitle(r.getFileName());
                        // Save new path as default storage path for records
                        defaultPath = r.getFullPath();
                        // reload files list
                        loadRecords(r.getFullPath());
                    } else {
                        startRecordActivity(position);
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                // highlight first selected card
                setIsInChoiceMode(true);
                switchSelectedState(position);
                // Start action mode
                actionMode = parent.startActionMode(new ActionBarCallback(parent));
                // Set title
                actionMode.setTitle(String.valueOf(getSelectedItemsCount()));
            }
        };
    }

    // Start blank RecorderActivity or pass to it record info from tapped list row
    private void startRecordActivity(int id) {
        Intent intent = new Intent(parent.getApplicationContext(), RecorderActivity.class);
        intent.putExtra(Constants.RECORD_EXTRA_ID, id);
        // Start activity called for create new record, pass chosen path to store it
        intent.putExtra(Constants.RECORD_EXTRA_DIR, defaultPath);

        parent.startActivityForResult(intent, RECORDER_ACTIVITY_RESULT);
    }

    /**
     * Dialog with confirmation to delete file or files
     * @param mode
     */
    public void actionDeleteFile(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(parent);
        builder.setTitle(R.string.delete_files_title)
                .setMessage(R.string.delete_files_message)
                // Delete files, close dialog and clear contextual action bar mode
                .setPositiveButton(R.string.delete_files_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (int idx : parent.getFilesListPresenter().getSelectedItems()) {
                            // Delete from storage
                            if(parent.getRecordsListApp().getStorageHelper().deleteFileFolder(
                                    parent.getFilesListPresenter().getFile(idx).getFullPath()) == true) {
                                // Remove file from list
                                deleteFile(idx);
                            } else {
                                Toast.makeText(parent, R.string.delete_files_error_deleting + getFile(idx).getFullPath(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        mode.finish();
                    }
                })
                // Close dialog and clear contextual action bar mode
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mode.finish();
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Dialog to create folder
     */
    public void actionCreateFolder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(parent);
        View dialogFolderCreate = parent.getLayoutInflater().inflate(R.layout.dialog_create_folder, null);
        final EditText folderName = (EditText)dialogFolderCreate.findViewById(R.id.create_folder_name);

        builder.setView(dialogFolderCreate)
                .setTitle(R.string.create_folder_title)
                .setPositiveButton(R.string.create_folder_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        // if user push create button with no folder name entered, just close dialog
                        if (folderName.getText().toString().isEmpty()) {
                            dialog.cancel();
                        } else {
                            // Compose new directory full path
                            String name = folderName.getText().toString();
                            String path = defaultPath+File.separator+name;
                            // Create folder
                            if(parent.getRecordsListApp().getStorageHelper().createFolder(path) == true) {
                                // Add folder to files list
                                // because folder size and last modification time not showed in list,
                                // so we don't care about values and fill it with zeroes
                                addFile(new FileHolder(name, path, 0, 0, TYPE_DIRECTORY));
                            } else {
                                Toast.makeText(parent, parent.getResources().getString(R.string.create_folder_error_creating) + path, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });

        final AlertDialog dialog = builder.create();

        // Validator to check if folder already exist
        folderName.addTextChangedListener(new FileNameValidator(folderName) {
            @Override
            public void validate(TextView view, String text) {
                // Compose folder full path
                String path = defaultPath + File.separator + text;
                // If file exist display warning and disable dialog positive button
                if(parent.getRecordsListApp().getStorageHelper().isFolderExist(path) == true) {
                    folderName.setError(parent.getResources().getString(R.string.create_folder_folder_already_exist));
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    folderName.setError(null);
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });

        dialog.show();
    }

    /**
     * Rename file or directory
     * @param mode
     */
    public void actionRenameFile(ActionMode mode) {
// todo
        Log.w(Constants.TAG, "actionRenameFile: ");
        editFile(0);
    }

    /**
     * Files list manipulation staff
     * load list from scratch, add, remove, update
     */
    public void loadRecords() {
        loadRecords(Constants.DEFAULT_RECORDS_PATH);
    }

    public void loadRecords(String path) {
        // make list of available files from shared storage folder
        // open shared storage and check if it is writable and we can save files to it
        // record files list and fill records
        if(parent.getRecordsListApp().getStorageHelper().isExternalStorageWritable() == true) {
            files = parent.getRecordsListApp().getStorageHelper().getStorageDir(path);
            if(adapter != null)
                adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(parent, R.string.navigator_storage_unavailable, Toast.LENGTH_SHORT);
        }
    }

    public void addFile(FileHolder file) {
        // todo: evaluate position
        int position = files.size();
        files.add(position, file);
        adapter.notifyItemInserted(position);
    }

    public void deleteFile(int index) {
        files.remove(index);
        adapter.notifyItemRemoved(index);
    }

    public void editFile(int index) {
        // todo: edit
        adapter.notifyItemChanged(index);
    }

    public FileHolder getFile(int position) {
        return files.get(position);
    }

    /**
     *
     * @param position
     * @param holder
     */
    public void onBindFilesRowViewAtPosition(int position, FilesCardView holder) {
        // Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        FileHolder file = files.get(position);
        // if type is file set appropriate icon and show modification date and size
        if(file.getType() == Constants.TYPE_FILE) {
            holder.setMimeIcon(R.mipmap.ic_file_green);
            holder.setSize(convertSize(file.getFileSize()));
            holder.setModified(timeStampToDate(file.getModified()));
            // for folder just set icon
        } else {
            holder.setMimeIcon(R.mipmap.ic_folder_red);
        }
        // if card selected change background color
        holder.setBackgroundColor(isSelected(position) == true ? selectedBackground.data : normalBackground.data);
        // fill card with file or folder information
        holder.setTitle(file.getFileName());
    }

    /**
     *
     * @return
     */
    public int getFilesRowCount() {
        return files.size();
    }

    /*
     *  methods for multichoice in records list recycler view
     */
    public boolean isSelected(int position) {
        return selected.contains(position);
    }

    public void switchSelectedState(int position) {
        if(isSelected(position) == true){
            // there are two methods remove(index) and remove(Object), because our object is int value
            // compiler wrongly uses remove(index) method
            // so we need to point it that we need to use remove(Object) method
            selected.remove((Object)position);
        } else {
            selected.add(position);
        }
        adapter.notifyItemChanged(position);
    }

    public void clearSelected() {
        int[] buf = getSelectedItems();
        selected.clear();
        for (int i : buf) {
            adapter.notifyItemChanged(i);
        }
    }

    public int getSelectedItemsCount() {
        return selected.size();
    }

    public int[] getSelectedItems() {
        int[] result = new int[selected.size()];
        for (int i = 0; i < selected.size(); i++) {
            result[i] = selected.get(i);
        }
        return result;
    }

    public void setIsInChoiceMode(boolean mode) {
        this.isInChoiceMode = mode;
    }

    public boolean getIsInChoiceMode() {
        return isInChoiceMode;
    }

    // Utils
    private String timeStampToDate(long timestamp) {
        // TODO: date format must be configurble via settings
        return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(timestamp));
    }

    // from http://stackoverflow.com/a/24805871
    public static String convertSize(long v) {
        if (v < 1024) return v + " B";
        int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
        return String.format("%.1f %sB", (double)v / (1L << (z*10)), " KMGTPE".charAt(z));
    }
}
