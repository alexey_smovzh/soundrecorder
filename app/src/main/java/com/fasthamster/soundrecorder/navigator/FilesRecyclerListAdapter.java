package com.fasthamster.soundrecorder.navigator;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fasthamster.soundrecorder.R;

/**
 * Created by alex on 15.04.17.
 */

public class FilesRecyclerListAdapter extends RecyclerView.Adapter<FilesCardHolder> {

   private final FilesListPresenter presenter;


    public FilesRecyclerListAdapter(FilesListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public FilesCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FilesCardHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.record_card, parent, false));
    }

    @Override
    public void onBindViewHolder(FilesCardHolder holder, int position) {
        presenter.onBindFilesRowViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getFilesRowCount();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
