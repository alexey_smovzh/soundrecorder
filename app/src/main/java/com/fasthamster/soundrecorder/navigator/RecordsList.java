package com.fasthamster.soundrecorder.navigator;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fasthamster.soundrecorder.R;
import com.fasthamster.soundrecorder.SoundRecorder;
import com.fasthamster.soundrecorder.settings.SettingsActivity;

import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_CANT_RECORD;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_EXTRA_ID;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_RESULT;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_WRONG_MIME_TYPE;

// http://www.techrepublic.com/blog/australian-technology/making-a-sortable-listview-in-android/
// https://developer.android.com/guide/topics/media/mediarecorder.html
// http://stackoverflow.com/questions/14295427/android-audio-recording-with-voice-level-visualization
// https://developer.android.com/guide/topics/data/data-storage.html
// http://www.sqlite.org/lang_createtrigger.html

// MVC links
// http://androidexample.com/Use_MVC_Pattern_To_Create_Very_Basic_Shopping_Cart__-_Android_Example/index.php?view=article_discription&aid=116
// https://coderwall.com/p/vfbing/passing-objects-between-activities-in-android
// http://stackoverflow.com/questions/15068870/observable-observer-android
// http://www.javaworld.com/article/2077258/learn-java/observer-and-observable.html
// https://andhradroid.wordpress.com/2012/04/05/object-observer-pattern-in-android/

// XML fab button with shadow
// http://stackoverflow.com/questions/26963219/how-to-create-a-floating-action-button-fab-in-android-using-appcompat-v21

// RecyclerView
// https://www.sitepoint.com/mastering-complex-lists-with-the-android-recyclerview/
// https://guides.codepath.com/android/using-the-recyclerview

// Toolbar & menus
// https://developer.android.com/guide/topics/ui/menus.html

// wave visualization
// https://github.com/Semantive/waveform-android
// https://github.com/EvanJRichter/AudioVisualizer/tree/master/Visualizer
// https://github.com/steelkiwi/AndroidRecording/tree/master/lib/src/com/skd/androidrecording/visualizer
// http://stackoverflow.com/questions/1339136/draw-text-in-opengl-es

// TODO: display available and used storage space
// TODO: settings (choose storage folder, name conversion, tune microphone, choose audio quality)
// TODO: remove ads with google payments
// TODO: voice to text prepaid feature with test period?

// todo: when recorder closed and user returns to navigator it not refresh folder content!!

// todo: rename activity and all classes to "navigator"
public class RecordsList extends AppCompatActivity {

    private SoundRecorder app;
    private RecordsListView view;
    private FilesListPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize view, presenter, subscribe to model
        app = (SoundRecorder)getApplication();
        view = new RecordsListView(this);
        presenter = new FilesListPresenter(this);
        // Subscribe to records model observable
        app.getRecordsModelObservable().addObserver(view);

    }
    // Getters
    public SoundRecorder getRecordsListApp() { return app; }
    public RecordsListView getRecordsListView() { return  view; }
    public FilesListPresenter getFilesListPresenter() { return presenter; }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_folder_add:
                // Show dialog for folder creation
                presenter.actionCreateFolder();
                break;
            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Result from run recorder activity
     * check result message and show right toast message
     * @param request
     * @param result
     * @param data
     */
    @Override
    protected void onActivityResult(int request, int result, Intent data) {
        if(request == RECORDER_ACTIVITY_RESULT) {
            if(result == RESULT_FIRST_USER) {
                // Getting Intent info
                int message = data.getIntExtra(RECORDER_ACTIVITY_EXTRA_ID, 0);
                switch (message) {
                    case RECORDER_ACTIVITY_CANT_RECORD:
                        Toast.makeText(this, R.string.recording_not_supported_message, Toast.LENGTH_LONG).show();
                        break;
                    case RECORDER_ACTIVITY_WRONG_MIME_TYPE:
                        Toast.makeText(this, R.string.wrong_mime_type_message, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
