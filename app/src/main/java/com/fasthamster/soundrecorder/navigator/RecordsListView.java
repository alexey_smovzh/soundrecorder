package com.fasthamster.soundrecorder.navigator;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.fasthamster.soundrecorder.R;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by alex on 15.04.17.
 */

// TODO: if creates file with the same name as already existing, in file system this file overwritten,
//       but in recycler view added it's duplicate

public class RecordsListView implements Observer {

    private RecordsList parent;

    private FloatingActionButton fabRecord;
    private RecyclerView recycler;


    public RecordsListView(RecordsList parent) {
        this.parent = parent;

        parent.setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) parent.findViewById(R.id.toolbar);
        parent.setSupportActionBar(toolbar);

        fabRecord = (FloatingActionButton) parent.findViewById(R.id.fab_record);
        recycler = (RecyclerView) parent.findViewById(R.id.rv_list_records);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable == parent.getRecordsListApp().getRecordsModelObservable()) {
            // todo: copy single item update method from recorder.RecorderView
//            parent.getFilesListPresenter().
//            adapter.update(parent.getRecordsListApp().getRecordsModelObservable().getRecords());
        }
    }

    public void registerFilesListAdapter(FilesRecyclerListAdapter adapter) {
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(parent));
    }

    public void registerFilesListClickListener(RecyclerViewOnClickListener listener) {
        recycler.addOnItemTouchListener(new RecyclerViewClickListener(parent, recycler, listener));
    }

    public void registerFabRecordListener(View.OnClickListener listener) {
        fabRecord.setOnClickListener(listener);
    }

    public void setActionBarTitle(String title) {
        parent.getSupportActionBar().setTitle(title);
    }
}
