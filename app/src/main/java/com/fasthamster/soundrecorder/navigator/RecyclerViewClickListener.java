package com.fasthamster.soundrecorder.navigator;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by alex on 15.04.17.
 */

public class RecyclerViewClickListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector detector;
    private RecyclerViewOnClickListener listener;


    public RecyclerViewClickListener(Context context, final RecyclerView view, final RecyclerViewOnClickListener listener) {
        this.listener = listener;

        detector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                //find the long pressed view
                View child = view.findChildViewUnder(e.getX(), e.getY());
                if (child != null && listener != null) {
                    listener.onLongClick(child, view.getChildLayoutPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
// todo: at android.support.v7.widget.RecyclerView.onInterceptTouchEvent(Unknown Source)

        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if(child != null && listener != null && detector.onTouchEvent(e)) {
            listener.onClick(child, rv.getChildLayoutPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
