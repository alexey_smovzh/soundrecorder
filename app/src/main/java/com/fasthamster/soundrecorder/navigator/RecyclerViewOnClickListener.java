package com.fasthamster.soundrecorder.navigator;

import android.view.View;

/**
 * Created by alex on 15.04.17.
 */

public interface RecyclerViewOnClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
