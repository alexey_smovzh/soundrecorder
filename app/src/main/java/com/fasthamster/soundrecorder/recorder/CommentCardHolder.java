package com.fasthamster.soundrecorder.recorder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasthamster.soundrecorder.Comment;
import com.fasthamster.soundrecorder.R;

/**
 * Created by alex on 07.06.17.
 */

public class CommentCardHolder extends RecyclerView.ViewHolder {

    private RecorderActivity parent;

    ImageView delete;
    TextView time, text;


    public CommentCardHolder(View view, RecorderActivity parent) {
        super(view);

        this.parent = parent;

        delete = (ImageView)view.findViewById(R.id.iv_comment_delete);
        time = (TextView)view.findViewById(R.id.tv_comment_time);
        text = (TextView)view.findViewById(R.id.tv_comment_text);

        delete.setOnClickListener(deleteClickListener);
        text.setOnLongClickListener(editTextListener);

    }

    private View.OnClickListener deleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            // Check if an item was deleted, but the user clicked it before the UI removed it
            if(position != RecyclerView.NO_POSITION) {
                parent.getRecordsListApp().getCommentsModelObservable().deleteComment(position);
            }
        }
    };

    private View.OnLongClickListener editTextListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                Comment comment = parent.getRecordsListApp().getCommentsModelObservable().getComment(position);
                parent.getRecordController().openTypeComment(position, comment.getText());
            }

            return true;
        }
    };
}



