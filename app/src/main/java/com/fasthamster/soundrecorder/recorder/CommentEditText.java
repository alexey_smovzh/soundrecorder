package com.fasthamster.soundrecorder.recorder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

/**
 * Created by alex on 11.06.17.
 */

/*
    Extends EditText onKeyPreIme method to overtake user hide soft keyboard event
    https://stackoverflow.com/questions/3425932/detecting-when-user-has-dismissed-the-soft-keyboard
 */

public class CommentEditText extends android.support.v7.widget.AppCompatEditText {

    private EditTextImeBackListener listener;

    public CommentEditText(Context context) {
        super(context);
    }

    public CommentEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CommentEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {

        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP)
            if(listener != null)
                listener.onImeBack(this);

        return super.dispatchKeyEvent(event);
    }

    public void setOnEditTextImeBackListener(EditTextImeBackListener listener) {
        this.listener = listener;
    }

    public interface EditTextImeBackListener {
        void onImeBack(CommentEditText edit);
    }
}
