package com.fasthamster.soundrecorder.recorder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasthamster.soundrecorder.Comment;
import com.fasthamster.soundrecorder.R;

import java.util.ArrayList;
import java.util.List;

import static com.fasthamster.soundrecorder.Constants.COMMENT_CARD_PERCENT_WIDTH;

/**
 * Created by alex on 07.06.17.
 */

public class CommentRecyclerListAdapter extends RecyclerView.Adapter<CommentCardHolder> {

    private RecorderActivity parent;
    private ArrayList<Comment> list = new ArrayList<>();

    public CommentRecyclerListAdapter(RecorderActivity parent, List<Comment> data) {
        this.parent = parent;
        this.list.addAll(data);
    }

    @Override
    public CommentCardHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_card, viewGroup, false);
        // Card width in percent
        // https://stackoverflow.com/questions/31776698/how-can-i-use-layout-weight-in-recyclerview-or-listview-item-view-to-have-items
        int width = viewGroup.getMeasuredWidth() / 100 * COMMENT_CARD_PERCENT_WIDTH;
        int height = viewGroup.getMeasuredHeight();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, height));
        CommentCardHolder holder = new CommentCardHolder(view, parent);

        return holder;
    }

    @Override
    public void onBindViewHolder(CommentCardHolder holder, int position) {
        Comment comment = list.get(position);
        holder.time.setText(comment.getTextTo());
        holder.text.setText(comment.getText());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateAll() {
        list.clear();
        list.addAll(parent.getRecordsListApp().getCommentsModelObservable().getComments());
        notifyDataSetChanged();
    }

    public void addItem(int index) {
        // in comments model already is a new data, so add it to adapter
        list.add(index, parent.getRecordsListApp().getCommentsModelObservable().getComment(index));
        notifyItemInserted(index);
    }

    public void editItem(int index) {
        list.set(index, parent.getRecordsListApp().getCommentsModelObservable().getComment(index));
        notifyItemChanged(index);
    }

    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public Comment getComment(int position) {
        return list.get(position);
    }

}
