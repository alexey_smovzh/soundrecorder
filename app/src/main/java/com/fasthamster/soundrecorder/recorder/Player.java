package com.fasthamster.soundrecorder.recorder;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.fasthamster.soundrecorder.R;
import com.fasthamster.soundrecorder.visualizer.GLVisualizerReadyListener;
import com.fasthamster.soundrecorder.visualizer.GLVisualizerSurfaceView;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import static com.fasthamster.soundrecorder.Constants.TAG;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDES_PER_SEGMENT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_REPEAT_INTERVAL;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_SECONDS_PER_SEGMENT;

/**
 * Created by alex on 11.04.17.
 */

/*  https://stackoverflow.com/questions/42455987/play-video-and-audio-using-mediacodec-and-mediaextractor
    https://stackoverflow.com/questions/43802167/generate-audio-graph-from-3gpp

    extractor seek https://github.com/protyposis/MediaPlayer-Extended/blob/master/MediaPlayer/src/main/java/net/protyposis/android/mediaplayer/MediaCodecVideoDecoder.java

    https://developer.android.com/reference/android/media/MediaCodec.html
    https://developer.android.com/reference/android/media/MediaExtractor.html
    https://developer.android.com/reference/android/media/MediaFormat.html
*/

public class Player implements GLVisualizerReadyListener {

    private RecorderActivity parent;
    private String path;

    private MediaPlayer player;
    private MediaExtractor extractor;
    private MediaCodec decoder;
    private ProgressDialog progress;

    private Thread extractionThread;

    private Handler handler;
    private Runnable updateVisualizer;
    private int loadedIndex;
    private int amplitudePlayed = 0;

    // 1000 * 1000 = 1 second
    private final static long ONE_INTERVAL = 1000 * 1000 / VISUALIZER_SECONDS_PER_SEGMENT;

    private boolean playing = false;


    /**
     * Constructor sets full path to desired file
     * @param parent
     * @param path full path to file
     */
    public Player(RecorderActivity parent, String path) {
        this.parent = parent;
        this.path = path;
    }

    /**
     * Run thread to read amplitude data from audio file
     */
    private void runAmplitudeExtractionThread() {

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            extractionThread = new runExtractor16();
        } else {
            extractionThread = new runExtractor21();
        }
        extractionThread.setPriority(Thread.MIN_PRIORITY);
        extractionThread.start();
    }

    /**
     * Create media player instance and try to play given in constructor file
     * if success return true, on exception return false
     *
     * On success run thread to create amplitudes visualisation
     *
     * @return
     */
    public boolean isPlayable() {
        player = new MediaPlayer();
        try {
            player.setDataSource(path);
            player.prepare();
            // Show message "please wait..."
            runProgressDialog();
        } catch (IOException e) {
            dispose();
            return false;
        }
        return true;
    }

    /**
     * Builds and shows wait dialog until extracted data for first segments
     */
    private void runProgressDialog() {
        progress = new ProgressDialog(parent);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(parent.getResources().getString(R.string.wait_until_amplitudes_loaded));
        progress.setIndeterminate(true);
        progress.show();
    }

    /**
     * This method invokes when GLVisualizer are ready to accept data
     * so start filling it
     */
    @Override
    public void visualizerReady() {

        runAmplitudeExtractionThread();

        // Wait until thread fill vith data first segment and render it
        while (loadedIndex < VISUALIZER_AMPLITUDES_PER_SEGMENT) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                parent.getRecordView().getVisualizer().requestRender();
            }
        }

        // Dismiss wait dialog
        progress.dismiss();

        // Setup handler for update played amplitudes
        handler = new Handler(Looper.getMainLooper());
        updateVisualizer = new Runnable() {
            @Override
            public void run() {
                // count running time
                amplitudePlayed++;
                // update played amplitude color
                parent.getRecordView().getVisualizer().markAmplitudePlayed(amplitudePlayed);
                // Schedule next run
                handler.postDelayed(this, VISUALIZER_REPEAT_INTERVAL);
            }
        };
    }

    /**
     *
     * @param listener
     */
    public void registerOnPlayFinishedListener(MediaPlayer.OnCompletionListener listener) {
        player.setOnCompletionListener(listener);
    }

    /**
     * Extract audio information from track with MediaExtractor
     * Decode extracted data to PCM raw data with MediaCodec
     * Calculates average max amplitude value from PCM data and add it to GLVisualizer
     * @throws IOException
     */
    @SuppressWarnings("deprecation")
    @TargetApi(16)
    private class runExtractor16 extends Thread {

        public void run() {
            try {
                extractor = new MediaExtractor();
                extractor.setDataSource(path);
                extractor.selectTrack(0);

                MediaFormat format = extractor.getTrackFormat(0);
                String mime = format.getString(MediaFormat.KEY_MIME);

                decoder = MediaCodec.createDecoderByType(mime);
                decoder.configure(format, null, null, 0);
                decoder.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

            MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();

            ByteBuffer[] inputBuffers = decoder.getInputBuffers();
            ByteBuffer[] outputBuffers = decoder.getOutputBuffers();

            GLVisualizerSurfaceView visualizer = parent.getRecordView().getVisualizer();

            long time = 0L;
            boolean gotInterval = false;
            short max = 0;

            while (isInterrupted() == false) {
                int input = decoder.dequeueInputBuffer(10000);
                if (input >= 0) {
                    ByteBuffer inBuffer = inputBuffers[input];
                    int chunk = extractor.readSampleData(inBuffer, 0);
                    if (chunk <= 0) {
                        decoder.queueInputBuffer(input, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        interrupt();
                    } else {
                        decoder.queueInputBuffer(input, 0, chunk, extractor.getSampleTime(), 0);
                        // evaluates if we read one amplitude interval to calculate max amplitude on it
                        if (extractor.getSampleTime() - time >= ONE_INTERVAL) {
                            gotInterval = true;
                            time += ONE_INTERVAL;
                        }
                        extractor.advance();
                    }
                }

                int output = decoder.dequeueOutputBuffer(info, 10000);
                if (output >= 0) {
                    if (info.size != 0) {
                        ByteBuffer outBuffer = outputBuffers[output];
                        outBuffer.position(0);
                        ShortBuffer pcm16Buffer = outBuffer.asShortBuffer();

                        while (pcm16Buffer.hasRemaining()) {
                            // calculates max amplitude value
                            short x = pcm16Buffer.get();
                            if(x > max)
                                max = x;

                            if (gotInterval == true) {
                                visualizer.addAmplitudeData(max);
                                max = 0;
                                loadedIndex++;
                                gotInterval = false;
                            }
                        }

                        pcm16Buffer.clear();
                        decoder.releaseOutputBuffer(output, false);

                        if (info.flags == MediaCodec.BUFFER_FLAG_END_OF_STREAM)
                            interrupt();

                    } else {
                        interrupt();
                    }
                } else if (output == -2) {
                    Log.w(TAG, "Output buffer is not available yet, feed more input");
                }
            }

            extractorDispose();
        }
    }

    /**
     * Extract audio information from track with MediaExtractor
     * Decode extracted data to PCM raw data with MediaCodec
     * Calculates average max amplitude value from PCM data and add it to GLVisualizer
     *
     * Difference from api 16 in input/output buffers creation
     * @throws IOException
     */
    @TargetApi(21)
    private class runExtractor21 extends Thread {

        public void run() {
            try {
                extractor = new MediaExtractor();
                extractor.setDataSource(path);
                extractor.selectTrack(0);

                MediaFormat format = extractor.getTrackFormat(0);
                String mime = format.getString(MediaFormat.KEY_MIME);

                decoder = MediaCodec.createDecoderByType(mime);
                decoder.configure(format, null, null, 0);
                decoder.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

            MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();

            GLVisualizerSurfaceView visualizer = parent.getRecordView().getVisualizer();

            long time = 0L;
            boolean gotInterval = false;
            short max = 0;

            while (isInterrupted() == false) {
                int input = decoder.dequeueInputBuffer(10000);
                if (input >= 0) {
                    ByteBuffer inBuffer = decoder.getInputBuffer(input);
                    int chunk = extractor.readSampleData(inBuffer, 0);
                    if (chunk <= 0) {
                        decoder.queueInputBuffer(input, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        interrupt();
                    } else {
                        decoder.queueInputBuffer(input, 0, chunk, extractor.getSampleTime(), 0);
                        // evaluates if we read one amplitude interval to calculate max amplitude on it
                        if (extractor.getSampleTime() - time >= ONE_INTERVAL) {
                            gotInterval = true;
                            time += ONE_INTERVAL;
                        }

                        extractor.advance();
                    }
                }

                int output = decoder.dequeueOutputBuffer(info, 1000000);
                if (output >= 0) {
                    if (info.size != 0) {
                        ByteBuffer outBuffer = decoder.getOutputBuffer(output);
                        outBuffer.position(0);
                        ShortBuffer pcm16Buffer = outBuffer.asShortBuffer();

                        while (pcm16Buffer.hasRemaining()) {
                            // calculates max amplitude value
                            short x = pcm16Buffer.get();
                            if(x > max)
                                max = x;

                            if (gotInterval == true) {
                                visualizer.addAmplitudeData(max);
                                max = 0;
                                loadedIndex++;
                                gotInterval = false;
                            }
                        }

                        pcm16Buffer.clear();
                        decoder.releaseOutputBuffer(output, false);

                        if (info.flags == MediaCodec.BUFFER_FLAG_END_OF_STREAM)
                            interrupt();

                    } else {
                        interrupt();
                    }
                } else if (output == -2) {
                    Log.w(TAG, "Output buffer is not available yet, feed more input");
                }
            }
            extractorDispose();
        }
    }

    /**
     * Run/Stop playing audio file
     */
    public void onPlay() {
        if(playing == false) {
            player.start();
            // rewind visualizer to player played amplitude position
            parent.getRecordView().getVisualizer().setPositionToAmplitude(amplitudePlayed);
            // run mark amplitudes played handler
            handler.post(updateVisualizer);
        } else {
            player.pause();
            handler.removeCallbacks(updateVisualizer);
        }
        playing = !playing;
    }

    /**
     * return playing state
     */
    public boolean isPlaying() {
        return playing;
    }

    /**
     * Prepare player for replay current audio
     */
    public void prepareForReplay() {
        playing = false;
        player.pause();
        handler.removeCallbacks(updateVisualizer);
        rewind(0);
    }

    /**
     *
     */
    public void rewindToBegin() {
        rewind(0);
    }

    /**
     *
     */
    public void rewindToEnd() {
        rewind(loadedIndex);
    }

    /**
     * rewind audio to position
     * @param position
     */
    private void rewind(int position) {
        amplitudePlayed = position;
        player.seekTo(position / 10 * 1000);        // convert position to msec
        parent.getRecordView().getVisualizer().setPositionToAmplitude(position);
        parent.getRecordView().getVisualizer().requestRender();
    }

    public void dispose() {
        if(player != null) {
            player.stop();
            player.release();
            player = null;
        }

        if(extractionThread != null)
            extractionThread.interrupt();

        if(handler != null)
            handler.removeCallbacks(updateVisualizer);

    }

    /**
     * Dispose extractor components after amplitudes extraction is done
     */
    @SuppressLint("NewApi")
    private void extractorDispose() {
        if(decoder != null){
            decoder.stop();
            decoder.release();
            decoder = null;
        }
        if(extractor != null) {
            extractor.unselectTrack(0);
            extractor.release();
            extractor = null;
        }
    }
}

