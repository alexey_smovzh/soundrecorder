package com.fasthamster.soundrecorder.recorder;

import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.fasthamster.soundrecorder.Constants;
import com.fasthamster.soundrecorder.visualizer.GLVisualizerReadyListener;

import java.io.IOException;

/**
 * Created by alex on 11.04.17.
 */

public class Recorder implements GLVisualizerReadyListener {

    private RecorderActivity parent;
    private MediaRecorder recorder = null;
    private String fileName = null;

    private Handler handler;
    private Runnable updateVisualizer;


    public Recorder(RecorderActivity parent, String fileName) {
        this.parent = parent;
        this.fileName = fileName;
    }

    /**
     * This method invokes when GLVisualizer are ready to accept data
     * so start filling it
     */
    @Override
    public void visualizerReady() {
        // todo: remove!!
//        final Random rand = new Random();
        // Setup handler and visualizer runnable
        handler = new Handler(Looper.getMainLooper());
        updateVisualizer = new Runnable() {
            @Override
            public void run() {
                int x = parent.getRecorder().getMaxAmplitude();
//                int x = rand.nextInt((int)Constants.VISUALIZER_MAX_AMPLITUDE);
                // Set last max amplitude
                parent.getRecordView().getVisualizer().addAmplitude(x);
                // Schedule next run
                handler.postDelayed(this, Constants.VISUALIZER_REPEAT_INTERVAL);
            }
        };
    }


    public void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    public int getMaxAmplitude() {
        return recorder.getMaxAmplitude();
    }

    /**
     * @return if visualizer has some amplitudes so app make some record - return truea
     */
    public boolean isRecord() {
        return parent.getRecordView().getVisualizer().getVisualizerTime() > 0L;
    }

    private void startRecording() {

        recorder = new MediaRecorder();
//        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        // TODO
        //recorder.setAudioSamplingRate();
        //recorder.setAudioChannels();
        //recorder.setAudioEncodingBitRate();


        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(Constants.TAG, e.toString());
        }

        handler.post(updateVisualizer);

        recorder.start();
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        handler.removeCallbacks(updateVisualizer);
        recorder = null;
    }

    public void dispose() {
        if (recorder != null) {
            recorder.stop();
            recorder.release();
            recorder = null;
        }

        if(handler != null)
            handler.removeCallbacks(updateVisualizer);
    }
}
