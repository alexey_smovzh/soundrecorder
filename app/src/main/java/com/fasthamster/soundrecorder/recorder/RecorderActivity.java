package com.fasthamster.soundrecorder.recorder;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.fasthamster.soundrecorder.SoundRecorder;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fasthamster.soundrecorder.Constants.COMMENT_FILE_EXTENSION;
import static com.fasthamster.soundrecorder.Constants.NO_RECORD;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_CANT_RECORD;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_EXTRA_ID;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_PLAYING_MODE;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_RECORD_MODE;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_WRONG_MIME_TYPE;
import static com.fasthamster.soundrecorder.Constants.RECORD_EXTRA_DIR;
import static com.fasthamster.soundrecorder.Constants.RECORD_EXTRA_ID;
import static com.fasthamster.soundrecorder.Constants.TEMPORARY_FILE_EXTENSION;
import static com.fasthamster.soundrecorder.Constants.TEMPORARY_FILE_NAME;

/**
 * Created by alex on 11.04.17.
 */

/*

  (flow 1)  >--- open existing file to Play no recording

  (flow 2)  >--- record new file---> Stop recording >-------------------------------------> Resume recording-->
                                 \                                                      /
  (flow 3)                        --> Start playing recorder file >---> Stop playing >--


   flow1   - create activity with play mode layout (without record button)
   flow2,3 - create activity with all buttons/switch between record/play modes??
   flow3 - reopen record activity in play mode?

 */

public class RecorderActivity extends AppCompatActivity {

    private SoundRecorder app;
    private RecorderView view;
    private RecorderController controller;
    private Recorder recorder;
    private Player player;

    private String recordName;
    private String commentName;
    private String recordPath;
    private String commentPath;


    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    // for storing values from intent
    private int id;
    private String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (SoundRecorder)getApplication();

        // Getting Intent info
        Intent intent = getIntent();
        id = intent.getIntExtra(RECORD_EXTRA_ID, NO_RECORD);
        path = intent.getStringExtra(RECORD_EXTRA_DIR);

        // Check records permission and setup recorder mode in onRequestPermissionsResult
        if(id == NO_RECORD) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO_PERMISSION);
        // open file for playing from path
        } else {
            setupPlayingMode(id);
        }
    }

    /**
     * Process result of requesting record permission
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            setupRecordingMode(path);
        } else {
            finishRecorderActivity(RECORDER_ACTIVITY_CANT_RECORD);
        }
    }

    /**
     * No record passed to activity, setup record mode
     * @param path full path to folder where user choose create record
     */
    public void setupRecordingMode(String path) {
        // Create name based on current date and time
        String date = new SimpleDateFormat("dd.MM-HHmmss").format(new Date(System.currentTimeMillis()));
        String temporary = TEMPORARY_FILE_NAME + date;
        recordName = temporary + TEMPORARY_FILE_EXTENSION;
        commentName = temporary + COMMENT_FILE_EXTENSION;
        recordPath = path + File.separator + recordName;
        commentPath = path + File.separator + commentName;

        // Setup recorder
        if(recorder != null)
            recorder.dispose();

        recorder = new Recorder(this, recordPath);

        setupActivityComponents(RECORDER_ACTIVITY_RECORD_MODE);
    }

    /**
     * Some records passed setup playing mode
     * @param id of passed record
     */
    private void setupPlayingMode(int id) {
        // Getting values for variables from record model by record id
        recordName = app.getRecordsModelObservable().getRecord(id).getFileName();
        commentName = app.getRecordsModelObservable().getRecord(id).getCommentName();
        recordPath = app.getRecordsModelObservable().getRecord(id).getFullPath();
        commentPath = app.getRecordsModelObservable().getRecord(id).getFullCommentPath();

        // Check if player can play given file
        // on exception return error code to navigator and finish activity
        player = new Player(this, recordPath);
        if(player.isPlayable() == false)
            finishRecorderActivity(RECORDER_ACTIVITY_WRONG_MIME_TYPE);

        // If all goes well setup activity components
        setupActivityComponents(RECORDER_ACTIVITY_PLAYING_MODE);
    }

    private void setupActivityComponents(byte mode) {
        // Load comments of a record
        app.getCommentsModelObservable().loadComments(commentPath);
        // Setup MVC activity components
        view = new RecorderView(this);
        controller = new RecorderController(this, mode);
        // Subscribe to records model observable
        app.getRecordsModelObservable().addObserver(view);
        // Subscribe to comments model observable
        app.getCommentsModelObservable().addObserver(view);
    }

    /**
     * Finish activity and pass result to navigator
     * @param message what action complete with error (see Constants for available codes)
     */
    public void finishRecorderActivity(int message) {
        Intent intent = new Intent();
        intent.putExtra(RECORDER_ACTIVITY_EXTRA_ID, message);
        setResult(RESULT_FIRST_USER, intent);
        finish();
    }

    /**
     * If record not null ask user to confirm exit and record lost
     */
    @Override
    public void onBackPressed() {
            controller.closeWithRecordLost();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(controller != null) controller.dispose();
        if(player != null) player.dispose();
        if(recorder != null) recorder.dispose();
    }

    // Getters
    public SoundRecorder getRecordsListApp() {
        return app;
    }
    public Recorder getRecorder() {
        return recorder;
    }
    public Player getPlayer() {
        return player;
    }
    public RecorderView getRecordView() {
        return view;
    }
    public RecorderController getRecordController() {
        return controller;
    }
    public String getPath() {
        return path;
    }
    public String getRecordPath() {
        return recordPath;
    }
    public String getRecordName() {
        return recordName;
    }
}