package com.fasthamster.soundrecorder.recorder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasthamster.soundrecorder.Constants;
import com.fasthamster.soundrecorder.FileNameValidator;
import com.fasthamster.soundrecorder.R;
import com.fasthamster.soundrecorder.Record;

import java.io.File;

import static com.fasthamster.soundrecorder.Constants.COMMENT_NO_EDITABLE_COMMENT;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_FINISH_WITHOUT_RECORD;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_FINISH_WITH_RECORD;
import static com.fasthamster.soundrecorder.Constants.RECORDER_ACTIVITY_PLAYING_MODE;

/**
 * Created by alex on 16.04.17.
 */

public class RecorderController {

    private RecorderActivity parent;
    private byte mode;

    private long timestamp = 0L;
    private int editableCommentIndex = COMMENT_NO_EDITABLE_COMMENT;

    private boolean recording = false;


    public RecorderController(final RecorderActivity parent, byte mode) {
        this.parent = parent;
        this.mode = mode;

        // Listeners for EditText to typing comments
        parent.getRecordView().registerTypeCommentActionListener(typeCommentListener);
        parent.getRecordView().registerTypeCommentImeBackListener(typeCommentImeBackListener);
        parent.getRecordView().registerTypeCommentFocusListener(typeCommentFocusListener);
        // Listeners for recorder/player buttons
        if (mode == RECORDER_ACTIVITY_PLAYING_MODE) {
            parent.getRecordView().setupPlayingButtonsMode(rewindButtonListener, playButtonListener, rewindButtonListener);
            parent.getPlayer().registerOnPlayFinishedListener(playerFinishPlayingListener);
            parent.getRecordView().setLeftButtonState(false);
            parent.getRecordView().setRightButtonState(true);
        } else {
            parent.getRecordView().setupRecordingButtonsMode(rerecordButtonListener, recordButtonListener, saveRecordButtonListener);
            // On activity launch buttons disabled. Enable if some record are made
            parent.getRecordView().setLeftButtonState(false);
            parent.getRecordView().setRightButtonState(false);
        }
        // set records name in view
        parent.getRecordView().setRecordName(parent.getRecordName());
        // setup listener when GLSurface will be created and visualizer be ready to accept data
        // pass class which implements interface depending on current mode
        parent.getRecordView().getVisualizer().getRenderer().getWave()
                .setOnGLVisualizerReadyListener(
                        mode == RECORDER_ACTIVITY_PLAYING_MODE ? parent.getPlayer() : parent.getRecorder());
    }

    public void dispose() {
        if(parent.getRecorder() != null)
            parent.getRecorder().dispose();

//        if(parent.getRecordView().getVisualizer() != null)
//            parent.getRecordView().getVisualizer().clear();

    }

    /**
     * Track EditText actions
     */
    private TextView.OnEditorActionListener typeCommentListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                saveComment(view.getText().toString());
                clearTypeComment((CommentEditText)view);
            }
            return false;
        }
    };

    /**
     *  Save timestamp on comment edit text get focus
     *  to provide time stamp label on moment when user decide to make comment, not when he finish typing
     */
    private View.OnFocusChangeListener typeCommentFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if(hasFocus == true)
                timestamp = parent.getRecordView().getVisualizer().getVisualizerTime();
        }
    };

    /**
     * Track hide keyboard action
     */
    private CommentEditText.EditTextImeBackListener typeCommentImeBackListener = new CommentEditText.EditTextImeBackListener() {
        @Override
        public void onImeBack(CommentEditText edit) {
            clearTypeComment(edit);
        }
    };

    /**
     * Clear comment edit text input, lost focus and hide soft keyboard
     */
    private void clearTypeComment(CommentEditText edit) {
        if(edit != null) {
            // Clear edit text
            edit.getText().clear();
            edit.clearFocus();
            // Set editable index to its default state
            editableCommentIndex = COMMENT_NO_EDITABLE_COMMENT;
            // Close soft keyboard
            InputMethodManager imm = (InputMethodManager) parent.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
        }
    }

    /**
     * Used by CommentCardHolder to open comment edit text and soft keyboard
     * @param text
     * @param index
     */
    public void openTypeComment(int index, String text) {
        editableCommentIndex = index;
        EditText edit = parent.getRecordView().getTypeCommentTextView();
        edit.setText(text);
        edit.requestFocus();
        // Show soft keyboard for the user to enter the value.
        InputMethodManager imm = (InputMethodManager)parent.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edit, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Add comment to comment model
     * @param text of comment
     */
    private void saveComment(String text) {
        // If text is empty just show the message that we can't add empty comment
        if(text.isEmpty()) {
            Toast.makeText(parent, R.string.add_comment_text_empty, Toast.LENGTH_SHORT).show();
        } else {
            // editableCommentIndex is -1 so we add new comment
            if(editableCommentIndex == COMMENT_NO_EDITABLE_COMMENT) {
                parent.getRecordsListApp().getCommentsModelObservable().addComment(timestamp, text);
            // editableCommentIndex is not - 1 so we edit existing comment
            } else {
                parent.getRecordsListApp().getCommentsModelObservable().editComment(editableCommentIndex, text);
            }
        }
    }

    /**
     *
     */
    private View.OnClickListener saveRecordButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // this button will be enabled only when some record are made
            // so don't need to check if some amplitudes was recorded
            actionSaveRecord();
        }
    };

    /**
     * Create dialog to ask user for record name
     * if user enters name and click positive button
     * rename temp file to provided name
     * @return
     */
    private void actionSaveRecord() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(parent);
        final View dialogSaveRecord = parent.getLayoutInflater().inflate(R.layout.dialog_save_record, null);
        final EditText recordName = (EditText)dialogSaveRecord.findViewById(R.id.save_record_name);

        builder.setView(dialogSaveRecord)
                .setTitle(R.string.save_record_title)
                .setPositiveButton(R.string.save_record_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        // if user don't provide input just close dialog
                        if(recordName.getText().toString().isEmpty()) {
                            dialog.cancel();
                        } else {
                            // Compose new directory full path
                            String name = recordName.getText().toString();
                            String path = parent.getPath() + File.separator + name;
                            // rename temp file
                            // rename temp file
                            if(parent.getRecordsListApp().getStorageHelper().renameFileFolder(parent.getRecordPath(), path) == true) {
                                File file = new File(path);
                                // add record to observer
                                parent.getRecordsListApp().getRecordsModelObservable().addRecord(new Record(name, path, file.length(), file.lastModified(), Constants.TYPE_FILE));
                                // return to navigator activity with record
                                parent.finishRecorderActivity(RECORDER_ACTIVITY_FINISH_WITH_RECORD);
                            } else {
                                Toast.makeText(parent, parent.getResources().getString(R.string.save_record_error_renaming) + path, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        final AlertDialog dialog = builder.create();

        // Validator to check if file already exist in user input
        recordName.addTextChangedListener(new FileNameValidator(recordName) {
            @Override
            public void validate(TextView view, String text) {
                // Compose file full path
                String path = parent.getPath() + File.separator + text;
                // If file exist display warning and disable dialog positive button
                if(parent.getRecordsListApp().getStorageHelper().isFileExist(path) == true) {
                    recordName.setError(parent.getResources().getString(R.string.save_record_file_already_exist));
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    recordName.setError(null);
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });

        dialog.show();
    }

    /**
     * Show dialog with message that record will be lost
     * on positive choice delete temporary file and close recorder activity
     */
    public void closeWithRecordLost() {
        // if record not null show confirmation dialog
        if(parent.getRecorder().isRecord() == true) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(parent);
            builder.setMessage(R.string.exit_record_message)
                    .setTitle(R.string.exit_record_title)
                    .setPositiveButton(R.string.exit_record_positive_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            parent.getRecordsListApp().getStorageHelper().deleteFileFolder(parent.getRecordPath());
                            parent.finishRecorderActivity(RECORDER_ACTIVITY_FINISH_WITHOUT_RECORD);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            final AlertDialog dialog = builder.create();
            dialog.show();

        } else {
            // Record is null exit without confirmation
            parent.finishRecorderActivity(RECORDER_ACTIVITY_FINISH_WITHOUT_RECORD);
        }
    }

    /**
     * Start recording at new button listener (left button)
     */
    private View.OnClickListener rerecordButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // confirmation dialog
            final AlertDialog.Builder builder = new AlertDialog.Builder(parent);
            builder.setMessage(R.string.restart_record_message)
                    .setTitle(R.string.restart_record_title)
                    .setPositiveButton(R.string.restart_record_positive_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            // Remove current temp file if exist
                            parent.getRecordsListApp().getStorageHelper().deleteFileFolder(parent.getRecordPath());
                            // Clear visualizer reinitialize recorder
                            parent.setupRecordingMode(parent.getPath());
                            // disable rerecord and save buttons
                            parent.getRecordView().setLeftButtonState(false);
                            parent.getRecordView().setRightButtonState(false);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    };

    /**
     *
     */
    private View.OnClickListener recordButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(recording == false) {
                parent.getRecorder().onRecord(true);
                // rewind wave to end if it was moved
                parent.getRecordView().getVisualizer().rewind();
                // start posting sound wave amplitude data to visualizer
            } else {
                // add new record to record model
                File f = new File(parent.getRecordPath());
                parent.getRecordsListApp().getRecordsModelObservable().addRecord(new Record(parent.getRecordName(), parent.getRecordPath(), f.length(), f.lastModified(), Constants.TYPE_FILE));
                dispose();
            }
            // enable rerecord and save buttons
            parent.getRecordView().setLeftButtonState(true);
            parent.getRecordView().setRightButtonState(true);

            recording = !recording;
        }
    };

    /**
     * rewind listener
     */
    private View.OnClickListener rewindButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == parent.getRecordView().getLeftButton()) {
                parent.getPlayer().rewindToBegin();
                parent.getRecordView().setLeftButtonState(false);
                parent.getRecordView().setRightButtonState(true);
            }
            if(v == parent.getRecordView().getRightButton()) {
                parent.getPlayer().rewindToEnd();
                parent.getRecordView().setLeftButtonState(true);
                parent.getRecordView().setRightButtonState(false);
            }
        }
    };

    /**
     * Manage play button clicks
     */
    private View.OnClickListener playButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            parent.getPlayer().onPlay();
            if(parent.getPlayer().isPlaying() == true) {
                parent.getRecordView().setLeftButtonState(false);
                parent.getRecordView().setRightButtonState(false);
            } else {
                parent.getRecordView().setLeftButtonState(true);
                parent.getRecordView().setRightButtonState(true);
            }
        }
    };

    /**
     * Manage end of playing file situation
     */
    private MediaPlayer.OnCompletionListener playerFinishPlayingListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            // stop player and prepare for replay
            parent.getPlayer().prepareForReplay();
            // change play button icon from pause to play
            parent.getRecordView().setCentralButtonState(false);
            parent.getRecordView().setLeftButtonState(false);
            parent.getRecordView().setRightButtonState(true);
        }
    };
}