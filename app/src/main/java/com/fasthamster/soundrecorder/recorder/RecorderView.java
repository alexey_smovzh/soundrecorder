package com.fasthamster.soundrecorder.recorder;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.fasthamster.soundrecorder.R;
import com.fasthamster.soundrecorder.visualizer.GLVisualizerSurfaceView;

import java.util.Observable;
import java.util.Observer;

import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_ADD;
import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_DELETE;
import static com.fasthamster.soundrecorder.Constants.COMMENT_ACTION_EDIT;

/**
 * Created by alex on 16.04.17.
 */

public class RecorderView implements Observer {

    private RecorderActivity parent;

    private TextView recordName;
    private RecyclerView comments;
    private GLVisualizerSurfaceView visualizer;
    private CommentEditText typeComment;
    private ToggleButton centerButton;
    private Button leftButton, rightButton;

    private CommentRecyclerListAdapter adapter;


    public RecorderView(RecorderActivity parent) {
        this.parent = parent;

        parent.setContentView(R.layout.record_activity);

        recordName = (TextView)parent.findViewById(R.id.tv_record_name);
        visualizer = (GLVisualizerSurfaceView)parent.findViewById(R.id.sound_visualizer);
        comments = (RecyclerView)parent.findViewById(R.id.rv_list_comments);
        typeComment = (CommentEditText) parent.findViewById(R.id.et_add_comment);
        centerButton = (ToggleButton)parent.findViewById(R.id.btn_center);
        leftButton = (Button)parent.findViewById(R.id.btn_left);
        rightButton = (Button)parent.findViewById(R.id.btn_right);


        // Set ime action. If set it on xml it is not everywhere working
        typeComment.setImeActionLabel(parent.getResources().getString(R.string.add_comment_button), EditorInfo.IME_ACTION_DONE);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        comments.setHasFixedSize(true);

        adapter = new CommentRecyclerListAdapter(parent, parent.getRecordsListApp().getCommentsModelObservable().getComments());
        comments.setAdapter(adapter);
        // Horizontal orientation for comment cards
        comments.setLayoutManager(new LinearLayoutManager(parent, LinearLayoutManager.HORIZONTAL, false));

    }

    /**
     * This class subscribed to two observables Record and Comment
     * @param observable
     * @param data
     */
    @Override
    public void update(Observable observable, Object data) {
        // We got record observable
        if(observable == parent.getRecordsListApp().getRecordsModelObservable()) {
            // Records was updated
            // TODO:
        }
        // We got comment observable
        if(observable == parent.getRecordsListApp().getCommentsModelObservable()) {
            // Cast Object to int[]
            int[] args = (int[])data;
            // args[0] is action, args[1] is item index
            switch (args[0]) {
                case COMMENT_ACTION_ADD:
                    adapter.addItem(args[1]);
                    // Scroll to new card
                    comments.scrollToPosition(args[1]);
                    break;
                case COMMENT_ACTION_EDIT:
                    adapter.editItem(args[1]);
                    break;
                case COMMENT_ACTION_DELETE:
                    adapter.deleteItem(args[1]);
                    break;
            }
        }
    }

    /**
     * Setup button icons and listeners for playing mode
     * left button - rewind to begin
     * center button - play / stop
     * right button - rewind to end
     * @param leftButtonListener
     * @param centerButtonListener
     * @param rightButtonListener
     */
    public void setupPlayingButtonsMode(View.OnClickListener leftButtonListener,
                                        View.OnClickListener centerButtonListener,
                                        View.OnClickListener rightButtonListener) {
        setupButtonIcons(R.drawable.recorder_btn_rewind_begin,
                         R.drawable.recorder_btn_play,
                         R.drawable.recorder_btn_rewind_end);
        setupButtonListeners(leftButtonListener, centerButtonListener, rightButtonListener);

    }

    /**
     * Setup button icons and listeners for record mode
     * left button - start recording afresh, current record will be lost
     * center button - record / pause
     * right button - finish recording and save result
     * @param leftButtonListener
     * @param centerButtonListener
     * @param rightButtonListener
     */
    public void setupRecordingButtonsMode(View.OnClickListener leftButtonListener,
                                          View.OnClickListener centerButtonListener,
                                          View.OnClickListener rightButtonListener) {
        setupButtonIcons(R.drawable.recorder_btn_rerecord,
                         R.drawable.recorder_btn_record,
                         R.drawable.recorder_btn_done);
        setupButtonListeners(leftButtonListener, centerButtonListener, rightButtonListener);

    }

    /**
     * Assign drawables to button background
     * @param leftButtonDrawableId
     * @param centerButtonDrawableId
     * @param rightButtonDrawableId
     */
    private void setupButtonIcons(int leftButtonDrawableId, int centerButtonDrawableId, int rightButtonDrawableId) {
        leftButton.setBackgroundResource(leftButtonDrawableId);
        centerButton.setBackgroundResource(centerButtonDrawableId);
        rightButton.setBackgroundResource(rightButtonDrawableId);
    }

    /**
     * Setup button listeners
     * @param leftButtonListener
     * @param centerButtonListener
     * @param rightButtonListener
     */
    private void setupButtonListeners(View.OnClickListener leftButtonListener,
                                      View.OnClickListener centerButtonListener,
                                      View.OnClickListener rightButtonListener) {
        leftButton.setOnClickListener(leftButtonListener);
        centerButton.setOnClickListener(centerButtonListener);
        rightButton.setOnClickListener(rightButtonListener);
    }

    public void setRecordName(String name) {
        recordName.setText(name);
    }

    public void setCentralButtonState(boolean state) {
        centerButton.setChecked(state);
    }

    public void setLeftButtonState(boolean state) {
        leftButton.setEnabled(state);
        leftButton.setAlpha(state == true ? 1f : 0.5f);
    }

    public void setRightButtonState(boolean state) {
        rightButton.setEnabled(state);
        rightButton.setAlpha(state == true ? 1f : 0.5f);
    }

    public void registerTypeCommentActionListener(TextView.OnEditorActionListener listener) {
        typeComment.setOnEditorActionListener(listener);
    }

    public void registerTypeCommentImeBackListener(CommentEditText.EditTextImeBackListener listener) {
        typeComment.setOnEditTextImeBackListener(listener);
    }

    public void registerTypeCommentFocusListener(View.OnFocusChangeListener listener) {
        typeComment.setOnFocusChangeListener(listener);
    }

    public CommentEditText getTypeCommentTextView() {
        return typeComment;
    }

    public GLVisualizerSurfaceView getVisualizer() {
        return visualizer;
    }

    public Button getLeftButton() {
        return leftButton;
    }

    public Button getRightButton() {
        return rightButton;
    }

}
