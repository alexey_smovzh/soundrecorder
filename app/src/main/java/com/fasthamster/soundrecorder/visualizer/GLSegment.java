package com.fasthamster.soundrecorder.visualizer;

/**
 * Created by alex on 05.05.17.
 */

public class GLSegment {

    private GLWave parent;

    private GLVolumeLevelsGrid grid;
    private GLVolumeAmplitudes amplitudes;
    private GLTimeLabels labels;

    /**
     *
     */
    public GLSegment(GLWave parent) {

        this.parent = parent;

        grid = new GLVolumeLevelsGrid(parent.getWidth(), parent.getHeight(), parent.getSecondSegmentWidth());
        amplitudes = new GLVolumeAmplitudes(parent.getHeight(), parent.getSecondSegmentWidth());
        labels = new GLTimeLabels(parent.getSegmentsCount(), parent.getHeight(), parent.getSecondSegmentWidth());

    }

    /**
     *
     */
    public void draw() {
        grid.draw(parent.getPositionLocation(), parent.getColorLocation());
        amplitudes.draw(parent.getPositionLocation(), parent.getColorLocation());
        labels.draw(parent.getPositionLocation(), parent.getColorLocation(), parent.getTextureCoordLocation());
    }

    public void addAmplitude(int amplitude) {
        amplitudes.addAmplitude(amplitude);
    }

    public void playedAmplitude(int amplitude) {
        amplitudes.playedAmplitude(amplitude);
    }

    public void clearAmplitudePlayedState(int from) {
        amplitudes.setColorToDefault(from);
    }

    public int getAmplitudeCount() {
        return amplitudes.getAmplitudeCount();
    }
}
