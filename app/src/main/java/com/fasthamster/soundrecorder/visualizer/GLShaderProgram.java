package com.fasthamster.soundrecorder.visualizer;

import android.opengl.GLES20;
import android.util.Log;

import com.fasthamster.soundrecorder.Constants;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glUseProgram;

/**
 * Created by alex on 05.05.17.
 */

public class GLShaderProgram {

    private int positionLocation;
    private int colorLocation;
    private int projectionLocation;
    private int offsetLocation;
    private int textureLocation;
    private int textureCoordLocation;

    protected static final String U_MATRIX = "u_Matrix";
    protected static final String U_OFFSET = "u_Offset";
    protected static final String A_POSITION = "a_Position";
    protected static final String A_COLOR = "a_Color";
    protected static final String V_COLOR = "v_Color";
    protected static final String A_TEX_COORD = "a_texCoord0";
    protected static final String V_UV = "v_uv";
    protected static final String U_TEXTURE = "u_Texture";


    private final String VERTEX_SHADER =
                    "uniform mat4 " + U_MATRIX + ";" +
                    "uniform vec4 " + U_OFFSET + ";" +
                    "attribute vec4 " + A_POSITION + ";" +
                    "attribute vec4 " + A_COLOR  + ";" +
                    "attribute vec2 " + A_TEX_COORD + ";" +
                    "varying vec2 " + V_UV  + ";" +
                    "varying vec4 " + V_COLOR  + ";" +
                    "void main() {" +
                       V_COLOR  + " = " + A_COLOR  + ";" +
                       V_UV  + " = " + A_TEX_COORD + ";" +
                    "  gl_Position = " + U_MATRIX + " * (" + A_POSITION + " - " + U_OFFSET + ");" +
                    "}";

    private final String FRAGMENT_SHADER =
                    "precision mediump float;" +
                    "varying vec4 " + V_COLOR  + ";" +
                    "uniform sampler2D " + U_TEXTURE + ";" +
                    "varying vec2 " + V_UV  + ";" +
                    "void main() {" +
                    "  vec4 color = "+ V_COLOR + ";" +
                    "  vec4 labels = texture2D(" + U_TEXTURE + ", " + V_UV  + ");" +
                    /**
                     *  in labels vertex color are black, so if we multiply "labels.rgb * color.rgb" we get black color
                     *  if we add "labels.rgb + color.rgb" in pixels where labels and color colors are white we get value over 1.0
                     *  so we compare labels and color values and draw maximum
                     */
                    "  gl_FragColor = max(labels, color);" +
                    "}";


    protected final int program;

    protected GLShaderProgram() {

        // compile shaders
        int vert = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
        int frag = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);
        program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vert);
        GLES20.glAttachShader(program, frag);
        GLES20.glLinkProgram(program);

        // get shader attributes and uniforms location
        positionLocation = GLES20.glGetAttribLocation(program, A_POSITION);
        colorLocation = GLES20.glGetAttribLocation(program, A_COLOR);
        projectionLocation = GLES20.glGetUniformLocation(program, U_MATRIX);
        offsetLocation = GLES20.glGetUniformLocation(program, U_OFFSET);
        textureLocation = GLES20.glGetUniformLocation(program, U_TEXTURE);
        textureCoordLocation = GLES20.glGetAttribLocation(program, A_TEX_COORD);

    }

    public void useProgram() {
        glUseProgram(program);
    }

    // Setters
    public void setProjection(float[] matrix) {
        GLES20.glUniformMatrix4fv(projectionLocation, 1, false, matrix, 0);
    }

    public void setOffset(float[] offset) {
        GLES20.glUniform4fv(offsetLocation, 1, offset, 0);
    }

    public void setTexture() {
        GLES20.glUniform1i(textureLocation, 0);
    }

    // Getters
    public int getPositionLocation() { return positionLocation; }
    public int getColorLocation() { return colorLocation; }
    public int getTextureLocation() { return textureLocation; }
    public int getTextureCoordLocation() { return textureCoordLocation; }
    public int getProjectionLocation() { return projectionLocation; }
    public int getOffsetLocation() { return offsetLocation; }

    /**
     *
     * @param type
     * @param code
     * @return
     */
    private int loadShader(int type, String code) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, code);
        GLES20.glCompileShader(shader);

        final int[] compileStatus = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, compileStatus, 0);
        Log.w(Constants.TAG, "Result of compiling source: \n" + code + "\n" + glGetShaderInfoLog(shader));

        return shader;
    }
}
