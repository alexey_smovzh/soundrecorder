package com.fasthamster.soundrecorder.visualizer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.HashMap;

import static com.fasthamster.soundrecorder.Constants.BYTES_PER_FLOAT;
import static com.fasthamster.soundrecorder.Constants.BYTES_PER_SHORT;
import static com.fasthamster.soundrecorder.Constants.COLORS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.COLORS_STRIDE;
import static com.fasthamster.soundrecorder.Constants.COORDS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.VERTICES_STRIDE;
import static com.fasthamster.soundrecorder.Constants.VISUALISER_LABELS_MARGIN;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_NUMBERS_RATIO;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_SECONDS_PER_SEGMENT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_TIME_LABEL_HEIGHT;

/**
 * Created by alex on 15.05.17.
 */

public class GLTimeLabels {
    // this segment index in GLWave segments array
    private int segmentIdx;
    private float secondSegmentWidth;
    // holds indices buffer size to use in draw call
    private int indicesBufferSize;
    private float numberWidth;
    private float numberHeight;
    // labels Y coordinate
    private final float y;
    private float x;

    private FloatBuffer vertices;
    private FloatBuffer textureCoords;
    private FloatBuffer colors;
    private ShortBuffer indices;

    // Special code to distinctive colon symbol from numbers
    public static final int COLON = 10;

    // UV coordinates 1f surface width / 10 numbers and colon symbol
    private final HashMap<Integer, float[]> UV_COORDS = new HashMap<Integer, float[]>() {{
        put(0, new float[]{ 0f, 0f,	0f, 1f,	0.091f, 1f,	0.091f, 0f,});
        put(1, new float[]{	0.091f, 0f,	0.091f, 1f,	0.182f, 1f,	0.182f, 0f,});
        put(2, new float[]{	0.182f, 0f,	0.182f, 1f,	0.273f, 1f,	0.273f, 0f,});
        put(3, new float[]{	0.273f, 0f,	0.273f, 1f,	0.364f, 1f,	0.364f, 0f,});
        put(4, new float[]{	0.364f, 0f,	0.364f, 1f,	0.455f, 1f,	0.455f, 0f,});
        put(5, new float[]{	0.455f, 0f,	0.455f, 1f,	0.546f, 1f,	0.546f, 0f,});
        put(6, new float[]{	0.546f, 0f,	0.546f, 1f,	0.637f, 1f,	0.637f, 0f,});
        put(7, new float[]{	0.637f, 0f,	0.637f, 1f,	0.728f, 1f,	0.728f, 0f,});
        put(8, new float[]{	0.728f, 0f,	0.728f, 1f,	0.819f, 1f,	0.819f, 0f,});
        put(9, new float[]{	0.819f, 0f,	0.819f, 1f,	0.91f, 1f,	0.91f, 0f,});
        put(COLON, new float[]{ 0.91f, 0f,	0.91f, 1f,	1f, 1f,	1f, 0f,});
    }};

    private static final int QUADS_PER_LABEL = 5;        // 4 for numbers and 1 for colon, format 00:00
    private static final int VERTICES_PER_QUAD = 4;
    private static final int COORDS_PER_QUAD = VERTICES_PER_QUAD * COORDS_PER_VERTEX;
    private static final int INDICIES_PER_QUAD = 6;
    private static final int TEXTURE_COORD_PER_QUAD = 8;


    /**
     *
     * @param idx - current segment index in GLWave segments array
     *              used to evaluate labels time range
     */
    public GLTimeLabels(int idx, float height, float secondSegmentWidth) {

        this.segmentIdx = idx;
        this.secondSegmentWidth = secondSegmentWidth;
        this.numberHeight = VISUALIZER_TIME_LABEL_HEIGHT;
        this.numberWidth = VISUALIZER_TIME_LABEL_HEIGHT * VISUALIZER_NUMBERS_RATIO;
        this.y = height * (1f - VISUALISER_LABELS_MARGIN);
        // segment width - five numbers of time label sprites
        // we gets free space left between segment border and time label
        // divide it to half to horizontally center time label
        this.x = (secondSegmentWidth - (numberWidth * 5)) / 2;

        allocateBuffers();
        fillBuffers();
    }

    public void draw(int positionLocation, int colorLocation, int textureCoordLocation) {
        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(positionLocation);
        GLES20.glVertexAttribPointer(positionLocation, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, VERTICES_STRIDE, vertices);
        // Texture coordinates
        GLES20.glEnableVertexAttribArray(textureCoordLocation);
        GLES20.glVertexAttribPointer(textureCoordLocation, 2, GLES20.GL_FLOAT, false, 0, textureCoords);
        // Color
        GLES20.glEnableVertexAttribArray(colorLocation);
        GLES20.glVertexAttribPointer(colorLocation, COLORS_PER_VERTEX, GLES20.GL_FLOAT, false, COLORS_STRIDE, colors);
        // Draw the square
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indicesBufferSize, GLES20.GL_UNSIGNED_SHORT, indices);
        // Disable vertex array
        GLES20.glDisableVertexAttribArray(positionLocation);
    }

    /**
     * based on segment number allocate buffers for time numbers for this segment
     * TODO: e.g. for 00:00 format or for 0:00:00 format if record length longer than 59 min
     */
    private void allocateBuffers() {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer buf = ByteBuffer.allocateDirect(VISUALIZER_SECONDS_PER_SEGMENT * QUADS_PER_LABEL * COORDS_PER_QUAD * BYTES_PER_FLOAT);
        buf.order(ByteOrder.nativeOrder());
        vertices = buf.asFloatBuffer();
        vertices.position(0);
        // initialize byte buffer for the indices draw prder
        indicesBufferSize = VISUALIZER_SECONDS_PER_SEGMENT * QUADS_PER_LABEL * INDICIES_PER_QUAD;
        buf = ByteBuffer.allocateDirect(indicesBufferSize * BYTES_PER_SHORT);
        buf.order(ByteOrder.nativeOrder());
        indices = buf.asShortBuffer();
        indices.position(0);
        // Texture coordinates
        buf = ByteBuffer.allocateDirect(VISUALIZER_SECONDS_PER_SEGMENT * QUADS_PER_LABEL * TEXTURE_COORD_PER_QUAD * BYTES_PER_FLOAT);
        buf.order(ByteOrder.nativeOrder());
        textureCoords = buf.asFloatBuffer();
        textureCoords.position(0);
        // color buffer
        buf = ByteBuffer.allocateDirect(VISUALIZER_SECONDS_PER_SEGMENT * QUADS_PER_LABEL * COORDS_PER_QUAD * COLORS_PER_VERTEX * BYTES_PER_FLOAT);
        buf.order(ByteOrder.nativeOrder());
        colors = buf.asFloatBuffer();
        colors.position(0);
    }

    /**
     * Fill buffers with sprites with proper positions on the surface
     * and proper UV coordinates to display numbers
     */
    private void fillBuffers() {
        // evaluate first 3 numbers, they be the same
        int startSecond = VISUALIZER_SECONDS_PER_SEGMENT * segmentIdx;
        int minutes =  startSecond / 60;
        int n0 = minutes / 10;
        int n1 = minutes - (n0 * 10);
        int n3 = (startSecond - (minutes * 60)) / 10;

        // increment last number
        for(int i = 0; i < VISUALIZER_SECONDS_PER_SEGMENT; i++) {
            addNumber(n0, x, y);
            addNumber(n1, x + numberWidth, y);
            addNumber(COLON, x + numberWidth * 2, y);       // always colon
            addNumber(n3, x + numberWidth * 3, y);
            addNumber(i, x + numberWidth * 4, y);

            x += secondSegmentWidth;
        }
    }

    /**
     * Add to vertices and indices array values for given number
     * positions of vertexes and UV coordinates
     *
     * @param number
     * @param x - sprite left bottom corner x position
     * @param y - sprite left bottom corner y position
     */
    // TODO: triangle strip
    private int idx = 0;        // to store last number index
    private void addNumber(int number, float x, float y) {
        int v = idx * COORDS_PER_QUAD;
        // Add square coordinates to vertices
        vertices.put(v, x);
        vertices.put(v + 1, y + numberHeight);
        vertices.put(v + 2, x);
        vertices.put(v + 3, y);
        vertices.put(v + 4, x + numberHeight);
        vertices.put(v + 5, y);
        vertices.put(v + 6, x + numberHeight);
        vertices.put(v + 7, y + numberHeight);

        int i = idx * INDICIES_PER_QUAD;
        int s = idx * 4;        // 4 is the square corners count
        // { 0, 1, 2, 0, 2, 3 }   { 4, 5, 6, 4, 6, 7 }
        // Add indices
        indices.put(i, (short)s);
        indices.put(i + 1, (short)(s + 1));
        indices.put(i + 2, (short)(s + 2));
        indices.put(i + 3, (short)s);
        indices.put(i + 4, (short)(s + 2));
        indices.put(i + 5, (short)(s + 3));

        // Add texture coord
        int c = idx * TEXTURE_COORD_PER_QUAD;
        float[] t = UV_COORDS.get(number);
        textureCoords.put(c, t[0]);
        textureCoords.put(c + 1, t[1]);
        textureCoords.put(c + 2, t[2]);
        textureCoords.put(c + 3, t[3]);
        textureCoords.put(c + 4, t[4]);
        textureCoords.put(c + 5, t[5]);
        textureCoords.put(c + 6, t[6]);
        textureCoords.put(c + 7, t[7]);

        // Add colors
        int f = idx * COORDS_PER_QUAD * COLORS_PER_VERTEX;
        colors.put(f, 0f);              // r
        colors.put(f + 1, 0f);          // g
        colors.put(f + 2, 0f);          // b
        colors.put(f + 3, 0f);          // a
        colors.put(f + 4, 0f);
        colors.put(f + 5, 0f);
        colors.put(f + 6, 0f);
        colors.put(f + 7, 0f);
        colors.put(f + 8, 0f);
        colors.put(f + 9, 0f);
        colors.put(f + 10, 0f);
        colors.put(f + 11, 0f);
        colors.put(f + 12, 0f);
        colors.put(f + 13, 0f);
        colors.put(f + 14, 0f);
        colors.put(f + 15, 0f);

        // increment numbers counter
        idx++;
    }
}
