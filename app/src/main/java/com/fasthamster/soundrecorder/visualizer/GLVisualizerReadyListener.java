package com.fasthamster.soundrecorder.visualizer;

/**
 * Created by alex on 24.06.17.
 */

public interface GLVisualizerReadyListener {
    void visualizerReady();
}
