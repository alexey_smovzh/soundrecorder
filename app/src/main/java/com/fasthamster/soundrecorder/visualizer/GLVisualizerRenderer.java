package com.fasthamster.soundrecorder.visualizer;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import com.fasthamster.soundrecorder.Constants;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


/**
 * Created by alex on 30.04.17.
 */

public class GLVisualizerRenderer implements GLSurfaceView.Renderer {

    private GLWave wave;
    // Orthographic projection
    private final float[] projectionMatrix = new float[16];


    public GLVisualizerRenderer() {
        this.wave = new GLWave();
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
       /*
        * By default, OpenGL enables features that improve quality but reduce
        * performance. One might want to tweak that especially on software
        * renderer.
        */
        GLES20.glDisable(GL10.GL_DEPTH_TEST);
        GLES20.glDisable(GL10.GL_DITHER);
        GLES20.glDisable(GL10.GL_LIGHTING);
        GLES20.glDisable(GLES20.GL_CULL_FACE);
        // enable bending
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc (GLES20.GL_ONE, GLES20.GL_ONE);

        wave.preload();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        // Orthographic projection matrix
        Matrix.orthoM(projectionMatrix, 0, 0f, (float)width, 0f, height, 0f, 1f);
        // pass surface width and height to calculate vertexes and indices buffers size
        wave.initialize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        GLES20.glClearColor(Constants.VISUALIZER_BACKGROUND_COLOR[0], Constants.VISUALIZER_BACKGROUND_COLOR[1],Constants.VISUALIZER_BACKGROUND_COLOR[2],Constants.VISUALIZER_BACKGROUND_COLOR[3]);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        wave.draw(projectionMatrix);
    }

    public void markAmplitudePlayed(int index) {
        wave.markAmplitudePlayed(index);
    }

    public void setPositionToAmplitude(int index) {
        wave.setPositionToAmplitude(index);
    }

    public void addAmplitudeData(short amplitude) {
        wave.addAmplitudeData(amplitude);
    }

    public void addAmplitude(int amplitude) {
        wave.addAmplitude(amplitude);
    }

    public void slideWave(float offset) {
        wave.slideWave(offset);
    }

    public GLWave getWave() {
        return wave;
    }

    public void rewind(GLVisualizerSurfaceView surface) {
        wave.rewind(surface);
    }

    public long getWaveTime() {
        return wave.getWaveTime();
    }

}
