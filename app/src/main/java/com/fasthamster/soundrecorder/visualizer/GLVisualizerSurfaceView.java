package com.fasthamster.soundrecorder.visualizer;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by alex on 30.04.17.
 */

public class GLVisualizerSurfaceView extends GLSurfaceView {

    private GLVisualizerRenderer renderer;

    private float prevX;


    public GLVisualizerSurfaceView(Context context) {
        this(context, null);
    }

    public GLVisualizerSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setEGLContextClientVersion(2);

        // GL renderer
        renderer = new GLVisualizerRenderer();
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    private float x;
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        x = event.getX();
        if(event.getAction() == MotionEvent.ACTION_MOVE)
            slideWave(x - prevX);

        prevX = x;

        return true;
    }

    public void markAmplitudePlayed(int index) {
        renderer.markAmplitudePlayed(index);
        requestRender();
    }

    public void setPositionToAmplitude(int index) {
        renderer.setPositionToAmplitude(index);
        requestRender();
    }

    public void addAmplitudeData(short amplitude) {
        renderer.addAmplitudeData(amplitude);
    }

    public void addAmplitude(int amplitude) {
        renderer.addAmplitude(amplitude);
        requestRender();
    }

    public void slideWave(float offset) {
        renderer.slideWave(offset);
        requestRender();
    }

    public long getVisualizerTime() {
        return renderer.getWaveTime();
    }

    public GLVisualizerRenderer getRenderer() {
        return renderer;
    }

    public void rewind() {
        renderer.rewind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
