package com.fasthamster.soundrecorder.visualizer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static com.fasthamster.soundrecorder.Constants.BYTES_PER_FLOAT;
import static com.fasthamster.soundrecorder.Constants.BYTES_PER_SHORT;
import static com.fasthamster.soundrecorder.Constants.COLORS_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.COLORS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.COLORS_STRIDE;
import static com.fasthamster.soundrecorder.Constants.COORDS_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.COORDS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.INDICES_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.VERTICES_STRIDE;
import static com.fasthamster.soundrecorder.Constants.VISUALISER_GRID_MARGIN;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDES_PER_SECOND;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDES_PER_SEGMENT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDE_COLOR;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_MAX_AMPLITUDE;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_PLAYED_AMPLITUDE_COLOR;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_WAVE_LINE_WIDTH;

/**
 * Created by alex on 15.05.17.
 */

public class GLVolumeAmplitudes {

    private FloatBuffer vertices;
    private FloatBuffer colors;
    private ShortBuffer indices;

    private int amplitudeCount = 0;
    private float step;

    private float amplitudeMaxHeight;
    private float verticalCenter;

    /**
     * Constructor
     * @param height used to scale amplitude to surface
     * @param width used for step evaluation
     */
    public GLVolumeAmplitudes(float height, float width) {

        this.step = width / VISUALIZER_AMPLITUDES_PER_SECOND;
        this.verticalCenter = height / 2f;
        // whole surface height divided to half minus top and bottom margin
        this.amplitudeMaxHeight = this.verticalCenter * (1f - VISUALISER_GRID_MARGIN * 2f);

        allocateBuffers();
    }

    /**
     *
     */
    public void draw(int positionLocation, int colorLocation) {
        GLES20.glLineWidth(VISUALIZER_WAVE_LINE_WIDTH);
        // Vertexes
        GLES20.glEnableVertexAttribArray(positionLocation);
        GLES20.glVertexAttribPointer(positionLocation, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, VERTICES_STRIDE, vertices);
        // Color
        GLES20.glEnableVertexAttribArray(colorLocation);
        GLES20.glVertexAttribPointer(colorLocation, COLORS_PER_VERTEX, GLES20.GL_FLOAT, false, COLORS_STRIDE, colors);
        // Draw the geometry
        GLES20.glDrawElements(GLES20.GL_LINES, amplitudeCount * 2, GLES20.GL_UNSIGNED_SHORT, indices);
        // Disable vertex array
        GLES20.glDisableVertexAttribArray(positionLocation);
    }
    /**
     * allocates buffers for amplitudes vertices and indices
     */
    private void allocateBuffers() {
        // prepare vertex buffer
        ByteBuffer vbuf = ByteBuffer.allocateDirect(VISUALIZER_AMPLITUDES_PER_SEGMENT * COORDS_PER_LINE * BYTES_PER_FLOAT); // 2 vertexes and 4 bytes per vertex
        vbuf.order(ByteOrder.nativeOrder());
        vertices = vbuf.asFloatBuffer();
        vertices.position(0);
        // prepare color buffer
        ByteBuffer cbuf = ByteBuffer.allocateDirect(VISUALIZER_AMPLITUDES_PER_SEGMENT * COLORS_PER_LINE * BYTES_PER_FLOAT);
        cbuf.order(ByteOrder.nativeOrder());
        colors = cbuf.asFloatBuffer();
        colors.position(0);
        // prepare indices buffer
        ByteBuffer ibuf = ByteBuffer.allocateDirect(VISUALIZER_AMPLITUDES_PER_SEGMENT * COORDS_PER_VERTEX * BYTES_PER_SHORT);
        ibuf.order(ByteOrder.nativeOrder());
        indices = ibuf.asShortBuffer();
        indices.position(0);
    }

    /**
     * Add vertexes and indices for sound wave with given amplitude
     * @param amplitude - value of last max amplitude value from recorder.getMaxAmplitude()
     */
    public void addAmplitude(int amplitude) {
        // divide amplitude value on max amplitude we gets normalized value in range 0..1
        // then multiply it to amplitude max height to scale it to surface size
        float normalizedY = (float)amplitude / VISUALIZER_MAX_AMPLITUDE * amplitudeMaxHeight;
        // normalize amplitudeCount
        float normalizedX = (float)amplitudeCount * step;

        setAmplitudeColor(amplitudeCount, VISUALIZER_AMPLITUDE_COLOR);
        setAmplitudeVertices(amplitudeCount, new float[]{normalizedX, verticalCenter - normalizedY, normalizedX, verticalCenter + normalizedY});

        int idx = amplitudeCount * INDICES_PER_LINE;
        indices.put(idx, (short)idx);
        indices.put(idx + 1, (short)(idx + 1));

        amplitudeCount++;
    }

    /**
     *
     * @param index
     */
    public void playedAmplitude(int index) {
        setAmplitudeColor(index, VISUALIZER_PLAYED_AMPLITUDE_COLOR);
    }

    /**
     * Reset all amplitudes color to default
     * @param from index of amplitude from which begin reset
     */
    public void setColorToDefault(int from) {
        for (int i = from; i < VISUALIZER_AMPLITUDES_PER_SEGMENT; i++)
            setAmplitudeColor(i, VISUALIZER_AMPLITUDE_COLOR);
    }

    /**
     * Set amplitude vertices
     * @param index
     * @param values
     */
    private void setAmplitudeVertices(int index, float[] values) {
        int v = index * COORDS_PER_LINE;
        vertices.put(v, values[0]);             // x coord
        vertices.put(v + 1, values[1]);         // y coord
        vertices.put(v + 2, values[2]);
        vertices.put(v + 3, values[3]);
    }
    /**
     * Change color of amplitude
     * @param index of amplitude
     * @param color to set
     */
    private void setAmplitudeColor(int index, float[] color) {
        int c = index * COLORS_PER_LINE;
        colors.put(c, color[0]);            // r
        colors.put(c + 1, color[1]);        // g
        colors.put(c + 2, color[2]);        // b
        colors.put(c + 3, color[3]);        // a
        colors.put(c + 4, color[0]);
        colors.put(c + 5, color[1]);
        colors.put(c + 6, color[2]);
        colors.put(c + 7, color[3]);
    }



    /**
     * Get amplitude count
     * @return
     */
    public int getAmplitudeCount() {
        return amplitudeCount;
    }
}
