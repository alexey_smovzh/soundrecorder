package com.fasthamster.soundrecorder.visualizer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static com.fasthamster.soundrecorder.Constants.BYTES_PER_FLOAT;
import static com.fasthamster.soundrecorder.Constants.BYTES_PER_SHORT;
import static com.fasthamster.soundrecorder.Constants.COLORS_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.COLORS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.COLORS_STRIDE;
import static com.fasthamster.soundrecorder.Constants.COORDS_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.COORDS_PER_VERTEX;
import static com.fasthamster.soundrecorder.Constants.INDICES_PER_LINE;
import static com.fasthamster.soundrecorder.Constants.VERTICES_STRIDE;
import static com.fasthamster.soundrecorder.Constants.VISUALISER_GRID_MARGIN;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_GRID_COLOR;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_GRID_LINE_WIDTH;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_SECONDS_PER_SEGMENT;

/**
 * Created by alex on 15.05.17.
 */
// todo: change grid lines to texture. it will be much better visual result
public class GLVolumeLevelsGrid {

    private FloatBuffer vertices;
    private FloatBuffer colors;
    private ShortBuffer indices;

    private int linesCount;
    private float secondSegmentWidth;

    private static final int HORIZONTAL_GLOBAL_LINES = 3;
    private static final int VERTICAL_LINES_PER_SEGMENT = 5;

    private final float topPoint, bottomPoint, leftPoint, rightPoint,
                        topSurfacePoint, bottomSurfacePoint;


    public GLVolumeLevelsGrid(float width, float height, float secondSegmentWidth) {
        this.secondSegmentWidth = secondSegmentWidth;
        this.linesCount = VERTICAL_LINES_PER_SEGMENT * VISUALIZER_SECONDS_PER_SEGMENT + HORIZONTAL_GLOBAL_LINES;
        // evaluate grid coordinate points
        this.leftPoint = 0f;
        this.rightPoint = width;
        this.topPoint = height * (1f - VISUALISER_GRID_MARGIN);
        this.bottomPoint = 0f;
        this.topSurfacePoint = height;
        this.bottomSurfacePoint = 0f;

        allocateBuffers();
        fillBuffers();
    }

    public void draw(int positionLocation, int colorLocation) {
        GLES20.glLineWidth(VISUALIZER_GRID_LINE_WIDTH);
        // Vertexes
        GLES20.glEnableVertexAttribArray(positionLocation);
        GLES20.glVertexAttribPointer(positionLocation, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, VERTICES_STRIDE, vertices);
        // Color
        GLES20.glEnableVertexAttribArray(colorLocation);
        GLES20.glVertexAttribPointer(colorLocation, COLORS_PER_VERTEX, GLES20.GL_FLOAT, false, COLORS_STRIDE, colors);
        // Draw the geometry
        GLES20.glDrawElements(GLES20.GL_LINES, linesCount * 2, GLES20.GL_UNSIGNED_SHORT, indices);
        // Disable vertex and color array
        GLES20.glDisableVertexAttribArray(positionLocation);
        GLES20.glDisableVertexAttribArray(colorLocation);
    }

    private void allocateBuffers() {
        // prepare vertex buffer
        ByteBuffer vbuf = ByteBuffer.allocateDirect(linesCount * COORDS_PER_LINE * BYTES_PER_FLOAT);
        vbuf.order(ByteOrder.nativeOrder());
        vertices = vbuf.asFloatBuffer();
        vertices.position(0);
        // prepare color buffer
        ByteBuffer cbuf = ByteBuffer.allocateDirect(linesCount * COLORS_PER_LINE * BYTES_PER_FLOAT);
        cbuf.order(ByteOrder.nativeOrder());
        colors = cbuf.asFloatBuffer();
        colors.position(0);
        // prepare indices buffer
        ByteBuffer ibuf = ByteBuffer.allocateDirect(linesCount * COORDS_PER_VERTEX * BYTES_PER_SHORT);
        ibuf.order(ByteOrder.nativeOrder());
        indices = ibuf.asShortBuffer();
        indices.position(0);
    }

    private void fillBuffers() {
        // horizontal lines (top, center, bottom)
        addLine(0, leftPoint, topPoint, rightPoint, topPoint);
        addLine(1, leftPoint, topSurfacePoint / 2f, rightPoint, topSurfacePoint / 2f);
        addLine(2, leftPoint, bottomPoint, rightPoint, bottomPoint);
        // Vertical lines, borders of 1 second segment
        for(int i = 0; i < VISUALIZER_SECONDS_PER_SEGMENT; i++) {
            addLine(i + HORIZONTAL_GLOBAL_LINES, i * secondSegmentWidth, topSurfacePoint, i * secondSegmentWidth, bottomPoint);
        }
    }

    private void addLine(int idx, float x0, float y0, float x1, float y1) {
        int v = idx * COORDS_PER_LINE;
        int c = idx * COLORS_PER_LINE;
        // first vertex
        vertices.put(v, x0);                                        // x
        vertices.put(v + 1, y0);                                    // y
        colors.put(c, VISUALIZER_GRID_COLOR[0]);          // r
        colors.put(c + 1, VISUALIZER_GRID_COLOR[1]);      // g
        colors.put(c + 2, VISUALIZER_GRID_COLOR[2]);      // b
        colors.put(c + 3, VISUALIZER_GRID_COLOR[3]);      // a
        // second vertex
        vertices.put(v + 2, x1);
        vertices.put(v + 3, y1);
        colors.put(c + 4, VISUALIZER_GRID_COLOR[0]);
        colors.put(c + 5, VISUALIZER_GRID_COLOR[1]);
        colors.put(c + 6, VISUALIZER_GRID_COLOR[2]);
        colors.put(c + 7, VISUALIZER_GRID_COLOR[3]);

        int i = idx * INDICES_PER_LINE;
        indices.put(i, (short)i);
        indices.put(i + 1, (short)(i + 1));
    }
}
