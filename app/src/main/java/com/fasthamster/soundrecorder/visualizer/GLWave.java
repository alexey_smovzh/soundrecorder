package com.fasthamster.soundrecorder.visualizer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.fasthamster.soundrecorder.SoundRecorder;

import java.util.ArrayList;

import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINEAR_MIPMAP_LINEAR;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDES_PER_SECOND;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_AMPLITUDES_PER_SEGMENT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_INITIALIZED_SEGMENTS_COUNT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_NUMBERS_RATIO;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_NUMBERS_TEXTURE;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_SECONDS_PER_SEGMENT;
import static com.fasthamster.soundrecorder.Constants.VISUALIZER_TIME_LABEL_HEIGHT;

/**
 * Created by alex on 01.05.17.
 */

// todo: select function


public class GLWave {

    private GLShaderProgram program;
    private ArrayList<GLSegment> segments = new ArrayList<>();
    private float secondSegmentWidth;
    private float width, height;
    private float halfWidth;
    private float slide = 0f;
    private boolean isSlided = false;
    private float step;
    private int currentSegmentIdx = 0;
    private float offset[] = {0f, 0f, 0f, 0f};
    private float leftOffset[] = {0f, 0f, 0f, 0f};
    private float rightOffset[] = {0f, 0f, 0f, 0f};

    private GLVisualizerReadyListener listener;


    /**
     * Setup shader and load numbers texture to display time labels
     */
    public void preload() {
        program = new GLShaderProgram();
        loadBindNumbersTexture(VISUALIZER_NUMBERS_TEXTURE);
    }

    /**
     * @param width surface width
     * @param height surface height
     */
    public void initialize(int width, int height) {
        this.width = (float)width;
        this.height = (float)height;

        // if evaluated one second width is lower than 150% of time label width
        // second width = 150% of time label width
        secondSegmentWidth = (float)width / VISUALIZER_SECONDS_PER_SEGMENT;
        // height/ratio is one number width, 5 is numbers count in time label
        float timeLabelWidth = VISUALIZER_TIME_LABEL_HEIGHT * VISUALIZER_NUMBERS_RATIO * 5 * 1.5f;
        if(secondSegmentWidth <= timeLabelWidth) {
            secondSegmentWidth = timeLabelWidth;
            // and if we stretch one segment size we need to stretch whole width too
            this.width = timeLabelWidth * VISUALIZER_SECONDS_PER_SEGMENT;
        }
        this.halfWidth = width / 2;
        step = secondSegmentWidth / VISUALIZER_AMPLITUDES_PER_SECOND;

        // initialize first two segments - Constants.VISUALIZER_INITIALIZED_SEGMENTS_COUNT
        // to solve problem of blank space when first segment will be moved over half of surface
        segments.add(new GLSegment(this));
        segments.add(new GLSegment(this));

        // fire up listener
        listener.visualizerReady();

    }

    /**
     *
     * @param projectionMatrix
     */
    public void draw(float[] projectionMatrix) {
        // Use shader
        program.useProgram();
        // Set global uniforms
        program.setProjection(projectionMatrix);
        program.setTexture();

        // draw current segment
        program.setOffset(offset);
        segments.get(currentSegmentIdx).draw();

        // TODO: check if segment visible. i.e. offset bigger or lower than center point
        // draw left segment if it exist
        if(currentSegmentIdx > 0) {
            leftOffset[0] = offset[0] + width;
            program.setOffset(leftOffset);
            segments.get(currentSegmentIdx-1).draw();

        }
        // draw right segment if it exist
        if(currentSegmentIdx + 1 < segments.size()) {
            rightOffset[0] = offset[0] - width;
            program.setOffset(rightOffset);
            segments.get(currentSegmentIdx+1).draw();
        }
    }

    public void setOnGLVisualizerReadyListener(GLVisualizerReadyListener listener) {
        this.listener = listener;
    }

    // Getters
    public float getWidth() {
        return width;
    }
    public float getHeight() {
        return height;
    }
    public int getSegmentsCount() {
        return segments.size();
    }
    public float getSecondSegmentWidth() {
        return secondSegmentWidth;
    }
    public long getWaveTime() {
        // if slide not reach half a screen width, we cant measure seconds by slide
        // so evaluate it by amplitude count on first segment
        if(slide == 0) {
            return (long)(segments.get(0).getAmplitudeCount() * step / width * VISUALIZER_SECONDS_PER_SEGMENT * 1000);
        } else {
            // slide is total width of added amplitudes
            // but this is abstract value and we dont know how many it in seconds
            // so we bound it with surface width to evaluate seconds
            // slide/width - how many VISUALIZER_SECONDS_PER_SEGMENT segments in slide value
            return (long) ((slide + halfWidth) / width * VISUALIZER_SECONDS_PER_SEGMENT * 1000);
        }
    }
    // getters for shader variables
    public int getPositionLocation() {
        return program.getPositionLocation();
    }
    public int getColorLocation() {
        return program.getColorLocation();
    }
    public int getTextureCoordLocation() {
        return program.getTextureCoordLocation();
    }

    /**
     * Add amplitude data only, without updating segment position variables
     * used in player mode
     * @param amplitude
     */
    public void addAmplitudeData(short amplitude) {
        // current segment are loaded, add another one
        if(segments.get(lastSegmentIdx()).getAmplitudeCount() == VISUALIZER_AMPLITUDES_PER_SEGMENT)
            segments.add(new GLSegment(this));

        // add amplitude to segment buffers
        segments.get(lastSegmentIdx()).addAmplitude(amplitude);
    }

    /**
     * Change color of amplitude
     * move segment to center played amplitude
     * @param index
     */
    public void markAmplitudePlayed(int index) {
        // calculate segment index where amplitude with given index stored
        int segment = index / VISUALIZER_AMPLITUDES_PER_SEGMENT;
        // index of amplitude in evaluated segment
        int ampIndex = index - (segment * VISUALIZER_AMPLITUDES_PER_SEGMENT);
        // get index and amplitude and redraw color
        segments.get(segment).playedAmplitude(ampIndex);
        // if we finish play segment update current segment index and set offset to default
        if(ampIndex == 0) {
            offset[0] = -halfWidth;
            currentSegmentIdx++;
        }

        // move segment to one step left
        offset[0] += step;
        slide += step;
        // don't move grid until first segment will be played at half
        if(index <= VISUALIZER_AMPLITUDES_PER_SEGMENT / 2) {
            offset[0] = 0f;
            slide = 0f;
        }
    }

    /**
     * Set visualizer current segment and offset variables
     * to position visualizer to center of any given amplitude by index
     * @param index of amplitude
     */
    public void setPositionToAmplitude(int index) {
        // calculate segment index where amplitude with given index stored
        currentSegmentIdx = index / VISUALIZER_AMPLITUDES_PER_SEGMENT;
        // index of amplitude in evaluated segment
        int ampIndex = index - (currentSegmentIdx * VISUALIZER_AMPLITUDES_PER_SEGMENT);
        // if index < then half of first segment, so we don't need to move wave
        offset[0] = index <= VISUALIZER_AMPLITUDES_PER_SEGMENT / 2 ? 0f : ampIndex * step - halfWidth;
        // clear current segment
        segments.get(currentSegmentIdx).clearAmplitudePlayedState(ampIndex);
        // clear all next segments if any exists
        if(currentSegmentIdx < segments.size())
            for (int i = currentSegmentIdx; i < segments.size(); i++) {
                segments.get(i).clearAmplitudePlayedState(0);
            }
    }

    /**
     * Add amplitude and update params of segment position
     * @param amplitude value of amplitude stretch
     */
    public void addAmplitude(int amplitude) {
        // current segment are filled, add another one, assign offset and increment index
        if(segments.get(lastSegmentIdx()).getAmplitudeCount() == VISUALIZER_AMPLITUDES_PER_SEGMENT) {
            segments.add(new GLSegment(this));
            offset[0] = -halfWidth;
            currentSegmentIdx++;
        }
        // add amplitude to segment buffers
        segments.get(lastSegmentIdx()).addAmplitude(amplitude);
        // move segment to one step left
        offset[0] += step;
        slide += step;
        // don't move grid until first segment will be filled at half
        if(segments.get(0).getAmplitudeCount() <= VISUALIZER_AMPLITUDES_PER_SEGMENT / 2) {
            offset[0] = 0f;
            slide = 0f;
        }
    }

    /**
     *
     * @return index of segment which are now filling.
     *         minus 2 because in initialize method we added two blank segments
     */
    private int lastSegmentIdx() {
        return segments.size() - VISUALIZER_INITIALIZED_SEGMENTS_COUNT;
    }

    /**
     *
     * @param distance of user sliding on screen
     */
    public void slideWave(float distance) {
        // Slide
        slide -= distance;
        // left side stopper to avoid draw request of non existing segment
        if(slide < 0f)
            slide = 0f;
        // right side stopper
        // all segments size minus one, which may stay on the screen
        if(slide > (float)segments.size() * width - width)
            slide = (float)segments.size() * width - width;
        // normalized integer part = current segment index
        currentSegmentIdx = (int)Math.floor(slide / width);
        // fractional part = offset of segment
        offset[0] = slide - (currentSegmentIdx * width);
        // Set slided flag
        isSlided = true;
    }

    /**
     * Rewind waves to place last amplitude on center of the surface
     * @param surface to call method requestRender() after every iteration
     */
    public void rewind(GLVisualizerSurfaceView surface) {

        if(isSlided == true) {
            int index = segments.size() - VISUALIZER_INITIALIZED_SEGMENTS_COUNT;
            float filledSegmentsWidth = (float)index * this.width;
            float lastFilledSegmentWidth = (float)segments.get(index).getAmplitudeCount() * step - halfWidth;
            float totalFilledWidth = filledSegmentsWidth + lastFilledSegmentWidth;

            // TODO: interpolation animation
            float diff = totalFilledWidth - slide;

            // set params
            slide = totalFilledWidth;
            currentSegmentIdx = index;
            offset[0] = lastFilledSegmentWidth;
            // unset slided flag
            isSlided = false;
        }
    }

    /**
     * Load texture with numbers to draw time labels
     * @param resourceId id of texture from res/drawable
     */
    private void loadBindNumbersTexture(int resourceId) {
        final int[] textureObjectIds = new int[1];
        GLES20.glGenTextures(1, textureObjectIds, 0);

        if (textureObjectIds[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            // Read in the resource
            final Bitmap bitmap = BitmapFactory.decodeResource(SoundRecorder.getContext().getResources(), resourceId, options);

            // Bind to the texture in OpenGL
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GL_TEXTURE_2D, textureObjectIds[0]);

            // Set filtering: a default must be set, or the texture will be
            // black.
            GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            GLES20.glTexParameteri(GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);

            // Note: Following code may cause an error to be reported in the
            // ADB log as follows: E/IMGSRV(20095): :0: HardwareMipGen:
            // Failed to generate texture mipmap levels (error=3)
            // No OpenGL error will be encountered (glGetError() will return
            // 0). If this happens, just squash the source image to be
            // square. It will look the same because of texture coordinates,
            // and mipmap generation will work.
            GLES20.glGenerateMipmap(GL_TEXTURE_2D);

            // Recycle the bitmap, since its data has been loaded into
            // OpenGL.
            bitmap.recycle();

        } else {
            throw new RuntimeException("Error loading texture.");
        }
    }
}
