package com.fasthamster.soundrecorder;

import android.content.Context;

import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by alex on 06.06.17.
 */

public class CommentTest {

    private static final ArrayList<String> TEST_SUBTITLE1 = new ArrayList<>(Arrays.asList("00:00:22,000 --> 00:02:39,000",
                                                                                          "I'll teach thee Bugology, Ignatzes"));

    private static final ArrayList<String> TEST_SUBTITLE2= new ArrayList<>(Arrays.asList("02:25:32,000 --> 00:00:27,000",
                                                                                         "I'll teach thee Bugology, Ignatzes"));


    private static final long RESULT1 = 22000;
    private static final long RESULT2 = 159000;
    private static final long RESULT3 = 8732000;

    private static final String VALUE1 = "00:00:22,000";
    private static final String VALUE2 = "00:02:39,000";
    private static final String VALUE3 = "02:25:32,000";


    @Mock
    Context context;

    /**
     * create subtitle with test data
     * gets timestamps to check it conversion from string to long
     */
    @Test
    public void stringToTimestamp_validator() {

        Comment comment1 = new Comment(TEST_SUBTITLE1);
        Comment comment2 = new Comment(TEST_SUBTITLE2);

        assertThat(comment1.getFrom(), is(RESULT1));
        assertThat(comment1.getTo(), is(RESULT2));
        assertThat(comment2.getFrom(), is(RESULT3));

    }

    /**
     * create subtitle with test data
     * gets timestamps text to check it conversion from long to string
     */
    @Test
    public void timestampToString_validator() {

        Comment comment1 = new Comment(TEST_SUBTITLE1);
        Comment comment2 = new Comment(TEST_SUBTITLE2);

        assertThat(comment1.getTextFrom(), is(VALUE1));
        assertThat(comment1.getTextTo(), is(VALUE2));
        assertThat(comment2.getTextFrom(), is(VALUE3));

    }

}
